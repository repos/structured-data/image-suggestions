#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from chispa import assert_df_equality

from image_suggestions import unillustratable


def test_get_non_illustratable_item_ids(spark_session, mocker):
    mocker.patch(
        'image_suggestions.shared.load_wikidata_items_with_p31',
        return_value=spark_session.createDataFrame(
            [
                ('Q1', 'Q577'),
                ('Q2', 'Q42'),
                ('Q3', 'Q4167410'),
            ],
            ['item_id', 'value'],
        ),
    )
    expected = spark_session.createDataFrame([{'item_id': 'Q1'}, {'item_id': 'Q3'}])
    actual = unillustratable.get_non_illustratable_item_ids(spark_session, '2022-01-01')

    assert_df_equality(actual, expected)


def test_get_non_illustratable_sections(spark_session, mocker):
    mocker.patch(
        'image_suggestions.unillustratable.read_denylist_parquet',
        return_value=spark_session.createDataFrame(
            [
                ('aawiki', 'denied section'),
            ],
            ['wiki_db', 'section_heading'],
        ),
    )
    suggestions = spark_session.createDataFrame(
        [
            ('aawiki', 'Allowed section'),
            ('aawiki', 'Denied section'),
            ('bbwiki', 'Allowed section'),
            ('bbwiki', 'Denied section'),
        ],
        ['wiki_db', 'target_section_heading'],
    )
    expected = spark_session.createDataFrame(
        [
            # only aawiki's "Denied section" section is expected to show up,
            # in the same format in which it's present in the input dataframe
            ('aawiki', 'Denied section'),
        ],
        ['wiki_db', 'section_heading'],
    )
    actual = unillustratable.get_non_illustratable_sections(
        spark_session,
        '/path/to/parquet',
        suggestions,
        suggestions.wiki_db,
        suggestions.target_section_heading,
    )

    assert_df_equality(actual, expected)
