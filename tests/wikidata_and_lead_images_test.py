#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.
# Also ignore row order if necessary, not relevant.

import math

from chispa import assert_df_equality

from image_suggestions import shared, wikidata_and_lead_images


def _compute_p373_score(pages_in_category):
    # Private function, not a test

    return int(
        round(
            (1 / math.log(pages_in_category + 1))
            * wikidata_and_lead_images.MAX_SCORE
            / 1.443
        )
    )


def test_gather_categories_with_links(spark_session, mocker):
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_categories',
        return_value=spark_session.createDataFrame(
            [
                ('category_1', 111),
                ('category_2', 222),
                ('category_2', 333),
                ('category_2', 444),
                ('category_2', 555),
            ],
            ['cat_title', 'cat_pages'],
        ),
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_category_links',
        return_value=spark_session.createDataFrame(
            [
                (77, 'category_0'),
                (88, 'category_1'),
                (99, 'category_2'),
            ],
            ['page_id', 'cat_title'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('category_2', 333, 99),
            ('category_2', 222, 99),
            ('category_2', 444, 99),
            ('category_2', 555, 99),
            ('category_1', 111, 88),
        ],
        ['cat_title', 'cat_pages', 'page_id'],
    )
    actual = wikidata_and_lead_images.gather_categories_with_links(
        'sparkling', 'monthly'
    )

    assert_df_equality(actual, expected, ignore_row_order=True)


def test_gather_commons_with_reverse_p18(spark_session):
    commons_file_pages = spark_session.createDataFrame(
        [
            (111, 'commons_page_1'),
            (222, 'commons_page_2'),
            (333, 'commons_page_3'),
        ],
        ['page_id', 'page_title'],
    )
    wikidata_items_with_p18 = spark_session.createDataFrame(
        [
            ('Q555', 'commons_page_99'),
            ('Q666', 'commons_page_1'),
            ('Q777', 'commons_page_1'),
            ('Q888', 'commons_page_2'),
        ],
        ['item_id', 'value'],
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'Q666', shared.P18_TAG, wikidata_and_lead_images.MAX_SCORE),
            (111, 'Q777', shared.P18_TAG, wikidata_and_lead_images.MAX_SCORE),
            (222, 'Q888', shared.P18_TAG, wikidata_and_lead_images.MAX_SCORE),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    actual = wikidata_and_lead_images.gather_commons_with_reverse_p18(
        commons_file_pages, wikidata_items_with_p18
    )

    assert_df_equality(actual, expected, ignore_schema=True)


def test_gather_all_commons_with_reverse_p373(spark_session, mocker):
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.gather_categories_with_links',
        return_value=spark_session.createDataFrame(
            [
                ('category_a', 2, 111),
                ('category_b', 20, 111),
                ('category_b', 20, 222),
                ('category_b', 20, 333),
                ('category_linked_to_non_existent_file', 30, 777),
                ('category_not_linked_via_P373', 30, 888),
            ],
            ['cat_title', 'cat_pages', 'page_id'],
        ),
    )
    commons_file_pages = spark_session.createDataFrame(
        [
            (111, 'commons_page_1'),
            (222, 'commons_page_2'),
            (333, 'commons_page_3'),
            (888, 'commons_page_in_category_not_linked_via_P373'),
            (999, 'commons_page_not_in_category'),
        ],
        ['page_id', 'page_title'],
    )
    wikidata_items_with_p373 = spark_session.createDataFrame(
        [
            ('Q1', 'category_a'),
            ('Q2', 'category_b'),
            ('Q3', 'category_c'),
            ('Q4', 'non_existent_category'),
            ('Q5', 'category_linked_to_non_existent_file'),
        ],
        ['item_id', 'value'],
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'Q1', shared.P373_TAG, _compute_p373_score(2)),
            (111, 'Q2', shared.P373_TAG, _compute_p373_score(20)),
            (222, 'Q2', shared.P373_TAG, _compute_p373_score(20)),
            (333, 'Q2', shared.P373_TAG, _compute_p373_score(20)),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    actual = wikidata_and_lead_images.gather_all_commons_with_reverse_p373(
        'sparkling', 'monthly', commons_file_pages, wikidata_items_with_p373
    )

    assert_df_equality(actual, expected, ignore_schema=True)


def test_gather_all_articles_with_lead_images(spark_session, mocker):
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_commons_file_pages',
        return_value=spark_session.createDataFrame(
            [
                (1, 'commons_1'),
                (2, 'commons_2'),
                (3, 'commons_file_not_used_as_lead_image'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_lead_images',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'commons_1'),
                ('ruwiki', 20, 'commons_2'),
                ('ptwiki', 30, 'nonexistent_commons_page'),
            ],
            ['wiki_db', 'page_id', 'lead_image_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_non_commons_main_pages',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'article_1'),
                ('ruwiki', 20, 'article_2'),
                ('ptwiki', 30, 'article_with_broken_lead_image_link'),
                ('gawiki', 40, 'article_with_no_lead_image'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_wikidata_item_page_links',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'Q100'),
                ('ruwiki', 20, 'Q101'),
            ],
            ['wiki_db', 'page_id', 'item_id'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('ptwiki', 'article_1', 'Q100', 1),
            ('ruwiki', 'article_2', 'Q101', 2),
        ],
        ['article_wiki', 'article_title', 'item_id', 'commons_page_id'],
    )
    actual = wikidata_and_lead_images.gather_all_articles_with_lead_images(
        'sparkling', '2022-05-16', '2022-04'
    )

    assert_df_equality(actual, expected)


def test_filter_articles_with_lead_images(spark_session):
    articles_with_lead_images = spark_session.createDataFrame(
        [
            ('ptwiki', 'article_1', 'Q100', 1),
            ('ruwiki', 'article_2', 'Q101', 2),
            ('gawiki', 'article_3', 'Q101', 2),
            ('eswiki', 'main_page', 'Q666', 7),
            ('jawiki', 'article_9', 'Q101', 2),
            ('elwiki', 'frequent_page', 'Q42', 9),
        ],
        ['article_wiki', 'article_title', 'item_id', 'commons_page_id'],
    )
    qid_threshold = 2
    frequently_updated_page_qids = ['Q42', 'Q666']
    # Commons page ID becomes the first column due to the left anti-join on it
    expected = spark_session.createDataFrame(
        [
            (1, 'ptwiki', 'article_1', 'Q100'),
        ],
        ['commons_page_id', 'article_wiki', 'article_title', 'item_id'],
    )
    actual = wikidata_and_lead_images.filter_articles_with_lead_images(
        articles_with_lead_images,
        qid_threshold=qid_threshold,
        frequently_updated_page_qids=frequently_updated_page_qids,
    )

    assert_df_equality(actual, expected)


def test_add_link_counts(spark_session, mocker):
    articles_with_lead_images = spark_session.createDataFrame(
        [
            ('ptwiki', 'article_1', 'Q100', 1),
            ('ruwiki', 'article_2', 'Q101', 2),
            ('gawiki', 'article_3', 'Q101', 2),
        ],
        ['article_wiki', 'article_title', 'item_id', 'commons_page_id'],
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.load_pagelinks',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 'article_1', 1),
                ('ptwiki', 'article_1', 2),
                ('ptwiki', 'article_1', 3),
                ('ptwiki', 'article_1', 4),
                ('ruwiki', 'article_2', 11),
                ('ruwiki', 'article_2', 12),
                ('gawiki', 'article_3', 21),
            ],
            ['wiki_db', 'to_title', 'from_id'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            (1, 'Q100', 4, ['ptwiki']),
            (2, 'Q101', 3, ['gawiki', 'ruwiki']),
        ],
        ['commons_page_id', 'item_id', 'incoming_link_count_all', 'found_on'],
    )
    actual = wikidata_and_lead_images.add_link_counts(
        'sparkling', 'monthly', articles_with_lead_images
    )

    assert_df_equality(actual, expected, ignore_schema=True)


def test_build_wikidata(spark_session, mocker):
    commons_file_pages = spark_session.createDataFrame(
        [(111, 'commons_page_1')], ['page_id', 'page_title']
    )
    wikidata_items_with_p18 = spark_session.createDataFrame(
        [('Q1', 'commons_page_1')], ['item_id', 'value']
    )
    wikidata_items_with_p373 = spark_session.createDataFrame(
        [('Q2', 'category')], ['item_id', 'value']
    )
    p18 = spark_session.createDataFrame(
        [(111, 'Q666', shared.P18_TAG, wikidata_and_lead_images.MAX_SCORE)],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.gather_commons_with_reverse_p18',
        return_value=p18,
    )
    p373 = spark_session.createDataFrame(
        [(111, 'Q1', shared.P373_TAG, 5), (222, 'Q2', shared.P373_TAG, 55)],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.gather_commons_with_reverse_p373',
        return_value=p373,
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'Q666', shared.P18_TAG, wikidata_and_lead_images.MAX_SCORE),
            (111, 'Q1', shared.P373_TAG, 5),
            (222, 'Q2', shared.P373_TAG, 55),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    actual = wikidata_and_lead_images.build_wikidata(
        'sparkling',
        '2022-05-16',
        commons_file_pages,
        wikidata_items_with_p18,
        wikidata_items_with_p373,
    )

    assert_df_equality(actual, expected)
    wikidata_and_lead_images.gather_commons_with_reverse_p18.assert_called_with(
        commons_file_pages, wikidata_items_with_p18
    )
    wikidata_and_lead_images.gather_commons_with_reverse_p373.assert_called_with(
        'sparkling', '2022-04', commons_file_pages, wikidata_items_with_p373
    )


def test_build_lead_images(spark_session, mocker):
    snapshot = '2022-05-16'
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.gather_articles_with_lead_images'
    )
    mocker.patch(
        'image_suggestions.wikidata_and_lead_images.add_link_counts',
        return_value=spark_session.createDataFrame(
            [
                # count > 5_000 ==> score = 1_000
                ('Q100', 1, 10_000, ['enwiki', 'itwiki']),
                # count = 1 ==> score = 10 (minimum)
                ('Q101', 2, 1, ['frwiki']),
                # count <= 5_000 ==> score = 110 (1 + floor(count * 0.2 / 10)) * 10
                ('Q808', 3, 500, ['arwiki']),
            ],
            [
                'item_id',
                'commons_page_id',
                'incoming_link_count_all',
                shared.FOUND_ON_COLNAME,
            ],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            (1, 'Q100', shared.LEAD_IMAGE_TAG, 1_000, ['enwiki', 'itwiki']),
            (2, 'Q101', shared.LEAD_IMAGE_TAG, 10, ['frwiki']),
            (3, 'Q808', shared.LEAD_IMAGE_TAG, 110, ['arwiki']),
        ],
        [
            'page_id',
            'item_id',
            shared.TAG_COLNAME,
            shared.SCORE_COLNAME,
            shared.FOUND_ON_COLNAME,
        ],
    )
    actual = wikidata_and_lead_images.build_lead_images('sparkling', snapshot)

    assert_df_equality(actual, expected, ignore_schema=True)
    wikidata_and_lead_images.gather_articles_with_lead_images.assert_called()
