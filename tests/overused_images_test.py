#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from chispa import assert_df_equality

from image_suggestions import overused_images


def test_get_link_thresholds_per_wiki(spark_session, mocker):
    mocker.patch(
        'image_suggestions.overused_images.load_wiki_sizes',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 1),
                ('bwiki', 4000),
                ('cwiki', 8000),
                ('dwiki', 12000),
                ('ewiki', 16000),
                ('fwiki', 20000),
                ('gwiki', 30000),
                ('hwiki', 40000),
                ('iwiki', 50000),
                ('jwiki', 60000),
                ('kwiki', 70000),
                ('lwiki', 80000),
                ('mwiki', 90000),
                ('nwiki', 100000),
            ],
            ['wiki_db', 'size'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 1, 4),
            ('bwiki', 4000, 4),
            ('cwiki', 8000, 4),
            ('dwiki', 12000, 4),
            ('ewiki', 16000, 5),
            ('fwiki', 20000, 6),
            ('gwiki', 30000, 9),
            ('hwiki', 40000, 12),
            ('iwiki', 50000, 15),
            ('jwiki', 60000, 17),
            ('kwiki', 70000, 18),
            ('lwiki', 80000, 19),
            ('mwiki', 90000, 19),
            ('nwiki', 100000, 20),
        ],
        ['wiki_db', 'size', 'threshold'],
    )
    actual = overused_images.get_link_thresholds_per_wiki(spark_session, '2022-01')

    assert_df_equality(actual, expected)


def test_get(spark_session, mocker):
    mocker.patch(
        'image_suggestions.overused_images.get_link_thresholds_per_wiki',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 1, 4),
                ('bwiki', 20000, 6),
            ],
            ['wiki_db', 'size', 'threshold'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_imagelinks',
        return_value=spark_session.createDataFrame(
            [
                # fewer links than threshold in all wikis - not expected in output
                ('awiki', 1, 'image_title_1'),
                ('awiki', 2, 'image_title_1'),
                ('awiki', 3, 'image_title_1'),
                ('bwiki', 1, 'image_title_1'),
                ('bwiki', 2, 'image_title_1'),
                ('bwiki', 3, 'image_title_1'),
                ('bwiki', 4, 'image_title_1'),
                ('bwiki', 5, 'image_title_1'),
                # more links than threshold in any wiki - expected in output
                ('awiki', 10, 'image_title_2'),
                ('awiki', 11, 'image_title_2'),
                ('awiki', 12, 'image_title_2'),
                ('awiki', 13, 'image_title_2'),
                ('awiki', 14, 'image_title_2'),
            ],
            ['wiki_db', 'article_id', 'image_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (100, 'image_title_1'),
                (101, 'image_title_2'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 101, 'image_title_2'),
        ],
        ['wiki_db', 'page_id', 'page_title'],
    )
    actual = overused_images.get(spark_session, '2022-01')

    assert_df_equality(actual, expected)
