#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.
# Also ignore row order if necessary, not relevant.

from chispa import assert_df_equality
from pyspark.sql import types as T

from image_suggestions import entity_images


def test_get(spark_session, mocker):
    mocker.patch(
        'image_suggestions.shared.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1.jpg'),
                (222, 'commons_page_2.jpg'),
                (333, 'commons_page_3.jpg'),
                (444, 'commons_page_4.jpg'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.entity_images.get_wikidata',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                (111, 'Q10', ['image.linked.from.wikidata.p18']),
                # b) in lead image and wikidata
                (222, 'Q20', ['image.linked.from.wikidata.p373']),
                # e) in wikidata and depicts
                (444, 'Q40', ['image.linked.from.wikidata.p373']),
                # f) in wikidata only
                (222, 'Q200', ['image.linked.from.wikidata.p18']),
                # an image that is not an illustration, should not appear in output
                (888, 'Q888', ['image.linked.from.wikidata.p18']),
            ],
            ['page_id', 'item_id', 'tag'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_lead_images',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                (111, 'Q10', ['awiki', 'bwiki']),
                # b) in lead image and wikidata
                (222, 'Q20', ['dwiki']),
                # c) in lead image and depicts
                (333, 'Q30', ['dwiki', 'ewiki']),
                # d) in lead image only
                (111, 'Q100', ['fwiki', 'gwiki']),
                # an image that is not an illustration, should not appear in output
                (777, 'Q777', ['pwiki', 'qwiki']),
            ],
            ['page_id', 'item_id', 'found_on'],
        ),
    )
    mocker.patch(
        'image_suggestions.entity_images.get_depicts',
        return_value=spark_session.createDataFrame(
            [
                # a) in all wikidata, lead image, depicts
                ('Q10', 111, ['P180']),
                # c) in lead image and depicts
                ('Q30', 333, ['P6243']),
                # e) in wikidata and depicts
                ('Q40', 444, ['P921']),
                # g) in depicts only
                ('Q300', 333, ['P180', 'P6243']),
                # an image that is not an illustration, should not appear in output
                ('Q999', 999, ['P6243', 'P921']),
            ],
            ['item_id', 'page_id', 'property_id'],
        ),
    )
    # separating out the expected values with a null 'found_on' field to make the dataframe easier to construct
    expected = spark_session.createDataFrame(
        [
            # a) in all wikidata, lead image, depicts
            (
                'Q10',
                'commons_page_1.jpg',
                ['awiki', 'bwiki'],
                ['istype-wikidata-image', 'istype-lead-image', 'istype-depicts'],
                99,
            ),
            # b) in lead image and wikidata
            (
                'Q20',
                'commons_page_2.jpg',
                ['dwiki'],
                ['istype-commons-category', 'istype-lead-image'],
                96,
            ),
            # c) in lead image and depicts
            (
                'Q30',
                'commons_page_3.jpg',
                ['dwiki', 'ewiki'],
                ['istype-lead-image', 'istype-depicts'],
                94,
            ),
            # d) in lead image only
            (
                'Q100',
                'commons_page_1.jpg',
                ['fwiki', 'gwiki'],
                ['istype-lead-image'],
                80,
            ),
            # e) in wikidata and depicts
            (
                'Q40',
                'commons_page_4.jpg',
                None,
                ['istype-commons-category', 'istype-depicts'],
                94,
            ),
            # f) in wikidata only
            ('Q200', 'commons_page_2.jpg', None, ['istype-wikidata-image'], 90),
            # g) in depicts only
            ('Q300', 'commons_page_3.jpg', None, ['istype-depicts'], 70),
        ],
        T.StructType(
            [
                T.StructField('item_id', T.StringType(), False),
                T.StructField('page_title', T.StringType(), False),
                T.StructField('found_on', T.ArrayType(T.StringType()), True),
                T.StructField('kind', T.ArrayType(T.StringType()), False),
                T.StructField('confidence', T.IntegerType(), False),
            ]
        ),
    ).orderBy('item_id', 'page_title')
    actual = entity_images.get(spark_session, 'beez', '2022-01-01')

    assert_df_equality(actual, expected, ignore_schema=True)


def test_get_with_limit(spark_session, mocker):
    mocker.patch(
        'image_suggestions.shared.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'commons_page_1.jpg'),
                (222, 'commons_page_2.jpg'),
                (333, 'commons_page_3.jpg'),
                (444, 'commons_page_4.jpg'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_lead_images',
        return_value=spark_session.createDataFrame(
            [
                (111, 'Q10', ['awiki', 'bwiki']),
                # b) in lead image and wikidata
                (222, 'Q10', ['dwiki']),
                # c) in lead image and depicts
                (333, 'Q10', ['dwiki', 'ewiki']),
                # d) in lead image only
                (111, 'Q20', ['fwiki', 'gwiki']),
            ],
            ['page_id', 'item_id', 'found_on'],
        ),
    )
    mocker.patch(
        'image_suggestions.entity_images.get_wikidata',
        return_value=spark_session.createDataFrame(
            [
                (111, 'Q10', ['image.linked.from.wikidata.p18']),
                (222, 'Q10', ['image.linked.from.wikidata.p373']),
                (444, 'Q20', ['image.linked.from.wikidata.p373']),
                (222, 'Q20', ['image.linked.from.wikidata.p18']),
            ],
            ['page_id', 'item_id', 'tag'],
        ),
    )
    mocker.patch(
        'image_suggestions.entity_images.get_depicts',
        return_value=spark_session.createDataFrame(
            [
                ('Q10', 111, ['P180']),
                ('Q20', 444, ['P180']),
                ('Q10', 333, ['P180', 'P6243']),
            ],
            ['item_id', 'page_id', 'property_id'],
        ),
    )
    # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
    expected = spark_session.createDataFrame(
        [
            (
                'Q10',
                'commons_page_1.jpg',
                ['awiki', 'bwiki'],
                ['istype-wikidata-image', 'istype-lead-image', 'istype-depicts'],
                99,
            ),
            (
                'Q10',
                'commons_page_2.jpg',
                ['dwiki'],
                ['istype-commons-category', 'istype-lead-image'],
                96,
            ),
            # commons_page_3.jpg absent for Q10 because of the limit
            (
                'Q20',
                'commons_page_4.jpg',
                None,
                ['istype-commons-category', 'istype-depicts'],
                94,
            ),
            ('Q20', 'commons_page_2.jpg', None, ['istype-wikidata-image'], 90),
            # commons_page_1.jpg absent for Q20 because of the limit
        ],
        T.StructType(
            [
                T.StructField('item_id', T.StringType(), False),
                T.StructField('page_title', T.StringType(), False),
                T.StructField('found_on', T.ArrayType(T.StringType()), True),
                T.StructField('kind', T.ArrayType(T.StringType()), False),
                T.StructField('confidence', T.IntegerType(), False),
            ]
        ),
    ).orderBy('item_id', 'page_title')
    actual = entity_images.get(spark_session, 'beez', '2022-01-01', limit_per_qid=2)

    assert_df_equality(actual, expected, ignore_schema=True, ignore_row_order=True)
