#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.
# Also ignore row order if necessary, not relevant.

from chispa import assert_df_equality

from image_suggestions import topic_slis


def test_get(spark_session, mocker):
    weekly_snapshot = '2023-02-02'
    mocker.patch(
        'image_suggestions.topic_slis.get_section_topics',
        return_value=spark_session.createDataFrame(
            [
                (
                    'awiki',
                    999999,
                    999,
                    'Page_title_9',
                    9,
                    'Section_title_9',
                    'Q9',
                    'Q999',
                ),
                (
                    'awiki',
                    888888,
                    888,
                    'Page_title_8',
                    8,
                    'Section_title_8',
                    'Q8',
                    'Q888',
                ),
                (
                    'awiki',
                    666666,
                    666,
                    'Page_title_6',
                    6,
                    'Section_title_6',
                    'Q6',
                    'Q666',
                ),
                (
                    'awiki',
                    555555,
                    555,
                    'Page_title_5',
                    5,
                    'Section_title_5',
                    'Q5',
                    'Q555',
                ),
                (
                    'awiki',
                    111111,
                    111,
                    'Page_title_1',
                    1,
                    'Section_title_1',
                    'Q1',
                    'Q111',
                ),
            ],
            [
                'wiki_db',
                'revision_id',
                'page_id',
                'page_title',
                'section_index',
                'section_title',
                'page_qid',
                'topic_qid',
            ],
        ),
    )
    mocker.patch(
        'image_suggestions.entity_images.get',
        return_value=spark_session.createDataFrame(
            [
                ('Q999', 'Image_999.jpg', None, ['istype-wikidata-image'], 90),
                ('Q888', 'Image_888.jpg', None, ['istype-commons-category'], 80),
                (
                    'Q777',
                    'Image_777.jpg',
                    None,
                    ['istype-commons-category', 'istype-wikidata-image'],
                    90,
                ),
                (
                    'Q666',
                    'Image_666.jpg',
                    ['aawiki'],
                    ['istype-wikidata-image', 'istype-depicts', 'istype-lead-image'],
                    90,
                ),
                ('Q555', 'Image_555.jpg', None, ['istype-depicts'], 70),
                ('Q444', 'Image_444.jpg', ['aawiki'], ['istype-lead-image'], 80),
                ('Q9', 'Image_999.jpg', ['aawiki'], ['istype-lead-image'], 80),
                ('Q6', 'Image_666.jpg', None, ['istype-depicts'], 70),
                ('Q1', 'Image_111.jpg', None, ['istype-commons-image'], 90),
            ],
            ['item_id', 'page_title', 'found_on', 'kind', 'confidence'],
        ),
    )
    actual = topic_slis.get(spark_session, 'test', weekly_snapshot, '/path/to/parquet')
    expected = spark_session.createDataFrame(
        [
            (
                'awiki',
                999999,
                999,
                'Page_title_9',
                9,
                'Section_title_9',
                'Image_999.jpg',
                'Q9',
                'Q999',
                'istype-section-topics',
                65,
            ),
            (
                'awiki',
                666666,
                666,
                'Page_title_6',
                6,
                'Section_title_6',
                'Image_666.jpg',
                'Q6',
                'Q666',
                'istype-section-topics',
                57,
            ),
        ],
        [
            'wiki_db',
            'target_page_rev_id',
            'target_page_id',
            'target_page_title',
            'target_section_index',
            'target_section_heading',
            'suggested_image',
            'target_qid',
            'topic_qid',
            'kind',
            'confidence',
        ],
    )

    assert_df_equality(actual, expected, ignore_schema=True, ignore_row_order=True)


def test_get_section_topics(spark_session, mocker):
    mocker.patch(
        'image_suggestions.topic_slis.read_section_topics_parquet',
        return_value=spark_session.createDataFrame(
            [
                (
                    '2023-02-02',
                    'awiki',
                    0,
                    123456,
                    'Q111',
                    789,
                    'Page_title_1',
                    1,
                    'Section_title_1',
                    'Q999',
                    'Topic_title',
                    1.1,
                ),
                (
                    '2023-02-02',
                    'bwiki',
                    0,
                    456789,
                    'Q222',
                    987,
                    'Page_title_2',
                    0,
                    'Section_title_2',
                    'Q888',
                    'Other_topic_title',
                    2.2,
                ),
            ],
            [
                'snapshot',
                'wiki_db',
                'page_namespace',
                'revision_id',
                'page_qid',
                'page_id',
                'page_title',
                'section_index',
                'section_title',
                'topic_qid',
                'topic_title',
                'topic_score',
            ],
        ),
    )
    actual = topic_slis.get_section_topics(spark_session, '/path/to/parquet', [0])
    expected = spark_session.createDataFrame(
        [
            (
                'awiki',
                123456,
                789,
                'Page_title_1',
                1,
                'Section_title_1',
                'Q111',
                'Q999',
            ),
        ],
        [
            'wiki_db',
            'revision_id',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'page_qid',
            'topic_qid',
        ],
    )

    assert_df_equality(actual, expected)
