#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.
# Also ignore row order if necessary, not relevant.

import pytest

from chispa import assert_df_equality
from pyspark.sql import functions as F
from pyspark.sql import types as T

from image_suggestions import alis, shared, slis


# NOTE This is complicated. See comments in `shared.get_monthly_snapshot`
def test_get_monthly_snapshot():
    # end of the week in this month, so latest monthly timestamp is last month
    assert shared.get_monthly_snapshot('2022-05-16') == '2022-04'
    assert shared.get_monthly_snapshot('2022-05-01') == '2022-04'
    assert shared.get_monthly_snapshot('2022-01-02') == '2021-12'
    # end of the week in next month, so timestamp is this month
    assert shared.get_monthly_snapshot('2022-05-30') == '2022-05'
    assert shared.get_monthly_snapshot('2021-12-27') == '2021-12'


def test_get_cirrus_index_snapshot():
    assert shared.get_cirrus_index_snapshot('1984-11-16') == '19841115'
    assert shared.get_cirrus_index_snapshot('2002-02-20') == '20020219'
    # Corner cases
    assert shared.get_cirrus_index_snapshot('1999-09-01') == '19990831'
    assert shared.get_cirrus_index_snapshot('2004-01-01') == '20031231'
    assert shared.get_cirrus_index_snapshot('2024-03-01') == '20240229'


def test_prepare_cirrus_index_tags(spark_session):
    relevant_tags = ('xpid', 'ypid', 'zpid', 'mpid', 'sugg')
    # As output by `shared.load_cirrus_index_tags`
    tags = spark_session.createDataFrame(
        [
            (
                'awiki',
                1,
                666,
                ['xpid/qid1|score1', 'xpid/qid2|score2', 'ypid/qid|score'],
            ),
            ('bwiki', 6, 610, ['mpid/qid|score', 'irrelevant_tag/value|score']),
            (
                'cwiki',
                9,
                999,
                ['xpid/qid9|score9', 'ypid/qid8|score8', 'zpid/qid7|score7'],
            ),
            ('dwiki', 3, 125, ['mpid/qid|score', 'mpid/qid|score']),
            ('ewiki', 1, 101, ['unwanted_tag/value|score', 'sugg/exists|1']),
            ('fwiki', 77, 88, None),
        ],
        ['wiki', 'namespace', 'page_id', 'weighted_tags'],
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 1, 666, 'xpid', ['qid1|score1', 'qid2|score2']),
            ('awiki', 1, 666, 'ypid', ['qid|score']),
            ('bwiki', 6, 610, 'mpid', ['qid|score']),
            ('cwiki', 9, 999, 'xpid', ['qid9|score9']),
            ('cwiki', 9, 999, 'ypid', ['qid8|score8']),
            ('cwiki', 9, 999, 'zpid', ['qid7|score7']),
            ('dwiki', 3, 125, 'mpid', ['qid|score', 'qid|score']),
            ('ewiki', 1, 101, 'sugg', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    actual = shared.prepare_cirrus_index_tags(tags, relevant_tags)

    assert_df_equality(actual, expected, ignore_schema=True)


def test_compute_search_index_delta(spark_session):
    previous = spark_session.createDataFrame(
        [
            # a) not in current, so should be deleted
            ('commonswiki', 6, 111, 'P18', ['Q123|1000']),
            # b) different sorting but identical in current and previous,
            #    so should be absent from delta
            ('commonswiki', 6, 112, 'P373', ['Q567|84', 'Q345|0', 'Q123|99']),
            # c) present in current
            ('commonswiki', 6, 113, 'P18', ['Q123|1000']),
            # d) different value from current
            ('commonswiki', 6, 212, 'lead_image', ['Q678|88']),
            # e) different page_id compared to current
            ('commonswiki', 6, 215, 'P18', ['Q678|88']),
            # f) different tag compared to current
            ('commonswiki', 6, 300, 'P18', ['Q789|22']),
            # a1) not in current, so should be deleted
            ('xwiki', 0, 111, 'recommendation.image', ['exists|1']),
            # b1) identical in current and previous, so should be absent from delta
            ('xwiki', 0, 112, 'recommendation.image', ['exists|1']),
            # a2) same in current and previous, so should be absent from delta
            ('y1wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y2wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y3wiki', 0, 555, 'recommendation.image', ['exists|1']),
            # a3) duplicate in previous
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    current = spark_session.createDataFrame(
        [
            # b) different sorting but identical in current and previous,
            #    so should be absent from delta
            ('commonswiki', 6, 112, 'P373', ['Q123|99', 'Q567|84', 'Q345|0']),
            # c) present in previous ... but also has 2 new values,
            #    so expect a new set containing 3 values
            ('commonswiki', 6, 113, 'P18', ['Q345|999', 'Q123|1000', 'Q567|1']),
            # d) different value from previous, should be updated
            ('commonswiki', 6, 212, 'lead_image', ['Q678|87']),
            # e) different page_id compared to previous, old page_id should be deleted and the new one added
            ('commonswiki', 6, 216, 'P18', ['Q678|1000']),
            # f) different tag compared to previous, old tag should be deleted and the new one added
            ('commonswiki', 6, 300, 'P373', ['Q789|22']),
            # g) absent from previous, so expected in delta
            ('commonswiki', 6, 333, 'lead_image', ['Q456|1000', 'Q567|99']),
            # b1) identical in current and previous, so should be absent from delta
            ('xwiki', 0, 112, 'recommendation.image', ['exists|1']),
            # c1) new in current, should be present in delta
            ('xwiki', 0, 113, 'recommendation.image', ['exists|1']),
            # a2) same in current and previous, so should be absent from delta
            ('y1wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y2wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y3wiki', 0, 555, 'recommendation.image', ['exists|1']),
            # a3) not duplicated in current, still should be absent from delta
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )

    # Test invalid target tags
    target_tags = []
    with pytest.raises(ValueError):
        shared.compute_search_index_delta(previous, current, target_tags)

    # Test delta with some Commons tags
    target_tags = ['P18', 'P373']
    expected = spark_session.createDataFrame(
        [
            # a)
            ('commonswiki', 6, 111, 'P18', ['__DELETE_GROUPING__']),
            # c) values are sorted
            ('commonswiki', 6, 113, 'P18', ['Q123|1000', 'Q345|999', 'Q567|1']),
            # e)
            ('commonswiki', 6, 215, 'P18', ['__DELETE_GROUPING__']),
            ('commonswiki', 6, 216, 'P18', ['Q678|1000']),
            # f)
            ('commonswiki', 6, 300, 'P18', ['__DELETE_GROUPING__']),
            ('commonswiki', 6, 300, 'P373', ['Q789|22']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    actual = shared.compute_search_index_delta(previous, current, target_tags)
    assert_df_equality(actual, expected)

    # Test delta with the ALIS tag
    target_tags = ['recommendation.image']
    expected = spark_session.createDataFrame(
        [
            # a1)
            ('xwiki', 0, 111, 'recommendation.image', ['__DELETE_GROUPING__']),
            # c1)
            ('xwiki', 0, 113, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    actual = shared.compute_search_index_delta(previous, current, target_tags)
    assert_df_equality(actual, expected)


def test_write_search_index_full(spark_session, mocker):
    mocker.patch('image_suggestions.shared.save_table')
    snapshot = '2022-05-11'
    expected = spark_session.createDataFrame(
        [
            (
                'commonswiki',
                6,
                10,
                'lead_image',
                ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200'],
                snapshot,
            ),
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19'], snapshot),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values', 'snapshot'],
    )
    actual = shared.save_search_index_full(expected, 'some_hive_db', snapshot, 2)

    assert_df_equality(actual, expected, ignore_schema=True)
    shared.save_table.assert_called()


def test_build_search_index_delta(spark_session, mocker):
    mocker.patch('image_suggestions.shared.load_cirrus_index_tags')
    mocker.patch('image_suggestions.shared.prepare_cirrus_index_tags')
    target_tags = ['lead_image', 'suggestion']
    snapshot = '2002-02-20'
    tags = spark_session.createDataFrame(
        [
            (
                'commonswiki',
                6,
                10,
                'lead_image',
                ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200'],
            ),
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
            ('ufowiki', 0, 666, 'suggestion', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )

    # Test delta without Commons
    delta_no_commons = spark_session.createDataFrame(
        [
            ('ufowiki', 0, 666, 'suggestion', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    # We're not testing the delta computation here, so just return the dataframe above
    mocker.patch(
        'image_suggestions.shared.compute_search_index_delta',
        return_value=delta_no_commons,
    )
    expected_no_commons = delta_no_commons.withColumn('snapshot', F.lit(snapshot))
    actual_no_commons = shared.build_search_index_delta(
        spark_session, tags, target_tags, snapshot, is_commons=False
    )
    assert_df_equality(actual_no_commons, expected_no_commons)

    # Test delta with Commons
    mocker.patch(
        'image_suggestions.shared.compute_search_index_delta', return_value=tags
    )
    expected_commons = tags.withColumn('snapshot', F.lit(snapshot))
    actual_commons = shared.build_search_index_delta(
        spark_session, tags, target_tags, snapshot
    )
    assert_df_equality(actual_commons, expected_commons)


def test_add_null_missing_columns(spark_session):
    # The schema must always be the same.
    # For some reason `createDataFrame` sets `section_index` to long
    # if we pass a plain list of columns, so pass the full schema instead.
    expected_schema = T.StructType(
        [
            T.StructField('a', T.StringType(), True),
            T.StructField('b', T.LongType(), True),
            T.StructField('c', T.DoubleType(), True),
            T.StructField('section_index', T.IntegerType(), True),
            T.StructField('section_heading', T.StringType(), True),
        ]
    )
    # Nothing to do if the dataframe has the target columns
    has_columns = spark_session.createDataFrame(
        [
            ('a', 1, 10.2, 666, 'section_heading1'),
            ('a', 1, 10.2, 1984, 'section_heading2'),
            ('a', 1, 10.2, 42, 'section_heading3'),
            ('a', 1, 10.2, 77, 'section_heading4'),
        ],
        expected_schema,
    )
    actual = shared.add_null_missing_columns(has_columns)

    assert_df_equality(actual, has_columns)

    # Add missing columns with null values
    missing_columns = spark_session.createDataFrame(
        [
            ('a', 1, 10.2),
            ('a', 1, 10.2),
            ('a', 1, 10.2),
            ('a', 1, 10.2),
        ],
        ['a', 'b', 'c'],
    )
    expected = spark_session.createDataFrame(
        [
            ('a', 1, 10.2, None, None),
            ('a', 1, 10.2, None, None),
            ('a', 1, 10.2, None, None),
            ('a', 1, 10.2, None, None),
        ],
        expected_schema,
    )
    actual = shared.add_null_missing_columns(missing_columns)

    assert_df_equality(actual, expected)


def test_save_title_cache(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki',
                100,
                'awiki_page_title_100',
                'Q9',
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
                ['pwiki', 'qwiki'],
            ),
            (
                'bwiki',
                100,
                'bwiki_page_title_100',
                'Q8',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                23456,
                ['xwiki'],
            ),
            (
                'bwiki',
                200,
                'bwiki_page_title_200',
                'Q7',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                34567,
                ['zwiki'],
            ),
            # different suggestion for the same page - should not result in duplicate entry in title cache
            (
                'bwiki',
                200,
                'bwiki_page_title_200',
                'Q7',
                fake_timeuuid,
                'commons_page_3',
                'commonswiki',
                90,
                ['istype-wikidata-image'],
                34567,
                [],
            ),
        ],
        [
            'wiki',
            'page_id',
            'page_title',
            'item_id',
            'id',
            'image',
            'origin_wiki',
            'confidence',
            'kind',
            'page_rev',
            'found_on',
        ],
    )

    expected = spark_session.createDataFrame(
        [
            (100, 12345, 'awiki_page_title_100', snapshot, 'awiki'),
            (100, 23456, 'bwiki_page_title_100', snapshot, 'bwiki'),
            (200, 34567, 'bwiki_page_title_200', snapshot, 'bwiki'),
        ],
        ['page_id', 'page_rev', 'title', 'snapshot', 'wiki'],
    )
    suggestions_clean = alis.prepare_for_saving(suggestions_full)
    actual = shared.save_title_cache(suggestions_clean, output_db, snapshot, coalesce)

    assert_df_equality(actual, expected, ignore_schema=True)
    shared.save_table.assert_called_with(
        actual,
        output_db,
        shared.TITLE_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )


def test_save_instanceof_cache(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki',
                100,
                'awiki_page_title_100',
                'Q9',
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
                ['pwiki', 'qwiki'],
            ),
            (
                'bwiki',
                100,
                'bwiki_page_title_100',
                'Q8',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                23456,
                ['xwiki'],
            ),
            # suggestion where the corresponding item_id doesn't have an instance_of value
            (
                'bwiki',
                200,
                'bwiki_page_title_200',
                'Q7',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                34567,
                ['zwiki'],
            ),
        ],
        [
            'wiki',
            'page_id',
            'page_title',
            'item_id',
            'id',
            'image',
            'origin_wiki',
            'confidence',
            'kind',
            'page_rev',
            'found_on',
        ],
    )
    mocker.patch(
        'image_suggestions.shared.load_wikidata_items_with_p31',
        return_value=spark_session.createDataFrame(
            [
                ('Q9', 'Q111'),
                ('Q9', 'Q222'),
                ('Q8', 'Q42'),
                # item/instanceof pair without corresponding article
                ('Q3', 'Q4167410'),
            ],
            ['item_id', 'value'],
        ),
    )
    expected_schema = T.StructType(
        [
            T.StructField('wiki', T.StringType(), True),
            T.StructField('page_id', T.LongType(), True),
            T.StructField('page_rev', T.LongType(), True),
            T.StructField('instance_of', T.ArrayType(T.StringType()), True),
            T.StructField('snapshot', T.StringType(), True),
        ]
    )
    expected = spark_session.createDataFrame(
        [
            ('awiki', 100, 12345, ['Q111', 'Q222'], snapshot),
            ('bwiki', 100, 23456, ['Q42'], snapshot),
            ('bwiki', 200, 34567, [], snapshot),
        ],
        ['wiki', 'page_id', 'page_rev', 'instance_of', 'snapshot'],
    )
    suggestions_clean = alis.prepare_for_saving(suggestions_full)
    actual = shared.save_instanceof_cache(
        spark_session, suggestions_clean, output_db, snapshot, coalesce
    )

    assert_df_equality(actual, expected, ignore_schema=True, ignore_row_order=True)
    shared.save_table.assert_called_with(
        actual,
        output_db,
        shared.INSTANCEOF_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )


def test_save_article_suggestions(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki',
                100,
                'awiki_page_title_100',
                'Q9',
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
                ['pwiki', 'qwiki'],
            ),
            (
                'bwiki',
                100,
                'bwiki_page_title_100',
                'Q8',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                23456,
                ['xwiki'],
            ),
            (
                'bwiki',
                200,
                'bwiki_page_title_200',
                'Q7',
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['istype-lead-image'],
                34567,
                ['zwiki'],
            ),
        ],
        [
            'wiki',
            'page_id',
            'page_title',
            'item_id',
            'id',
            'image',
            'origin_wiki',
            'confidence',
            'kind',
            'page_rev',
            'found_on',
        ],
    )
    expected_schema = T.StructType(
        [
            T.StructField('page_id', T.LongType(), True),
            T.StructField('id', T.StringType(), True),
            T.StructField('image', T.StringType(), True),
            T.StructField('origin_wiki', T.StringType(), True),
            T.StructField('confidence', T.LongType(), True),
            T.StructField('found_on', T.ArrayType(T.StringType()), True),
            T.StructField('kind', T.ArrayType(T.StringType()), True),
            T.StructField('page_rev', T.LongType(), True),
            T.StructField('section_heading', T.StringType(), True),
            T.StructField('snapshot', T.StringType(), False),
            T.StructField('wiki', T.StringType(), True),
            T.StructField('page_qid', T.StringType(), True),
            T.StructField('section_index', T.IntegerType(), True),
        ]
    )
    expected = spark_session.createDataFrame(
        [
            (
                100,
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['pwiki', 'qwiki'],
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
                None,
                snapshot,
                'awiki',
                'Q9',
                None,
            ),
            (
                100,
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['xwiki'],
                ['istype-lead-image'],
                23456,
                None,
                snapshot,
                'bwiki',
                'Q8',
                None,
            ),
            (
                200,
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['zwiki'],
                ['istype-lead-image'],
                34567,
                None,
                snapshot,
                'bwiki',
                'Q7',
                None,
            ),
        ],
        expected_schema,
    )
    suggestions_clean = alis.prepare_for_saving(suggestions_full)
    actual = shared.save_suggestions(suggestions_clean, output_db, snapshot, coalesce)

    assert_df_equality(actual, expected)
    shared.save_table.assert_called_with(
        actual,
        output_db,
        shared.SUGGESTIONS_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )


def test_save_section_suggestions(spark_session, mocker):
    output_db = 'test'
    mocker.patch('image_suggestions.shared.save_table')
    fake_timeuuid = 'xyz'
    snapshot = '2022-05-16'
    coalesce = 2
    suggestions_full = spark_session.createDataFrame(
        [
            (
                'awiki',
                'Q9',
                12345,
                100,
                'awiki_page_title_100',
                666,
                'I_M_section_title',
                'commons_page_1',
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                ['pwiki', 'qwiki'],
                90,
            ),
            (
                'bwiki',
                'Q8',
                23456,
                100,
                'bwiki_page_title_100',
                77,
                'Y_R_section_title',
                'commons_page_2',
                ['istype-lead-image'],
                ['xwiki'],
                90,
            ),
            (
                'bwiki',
                'Q7',
                34567,
                200,
                'bwiki_page_title_200',
                1984,
                'YA_sect_title',
                'commons_page_2',
                ['istype-lead-image'],
                ['zwiki'],
                90,
            ),
        ],
        [
            'wiki_db',
            'target_qid',
            'target_page_rev_id',
            'target_page_id',
            'target_page_title',
            'target_section_index',
            'target_section_heading',
            'suggested_image',
            'kind',
            'origin_wikis',
            'confidence',
        ],
    )
    expected_schema = T.StructType(
        [
            T.StructField('page_id', T.LongType(), True),
            T.StructField('id', T.StringType(), False),
            T.StructField('image', T.StringType(), True),
            T.StructField('origin_wiki', T.StringType(), False),
            T.StructField('confidence', T.LongType(), True),
            T.StructField('found_on', T.ArrayType(T.StringType()), True),
            T.StructField('kind', T.ArrayType(T.StringType()), True),
            T.StructField('page_rev', T.LongType(), True),
            T.StructField('section_heading', T.StringType(), True),
            T.StructField('snapshot', T.StringType(), False),
            T.StructField('wiki', T.StringType(), True),
            T.StructField('page_qid', T.StringType(), True),
            T.StructField('section_index', T.LongType(), True),
        ]
    )
    expected = spark_session.createDataFrame(
        [
            (
                100,
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['pwiki', 'qwiki'],
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
                'I_M_section_title',
                snapshot,
                'awiki',
                'Q9',
                666,
            ),
            (
                100,
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['xwiki'],
                ['istype-lead-image'],
                23456,
                'Y_R_section_title',
                snapshot,
                'bwiki',
                'Q8',
                77,
            ),
            (
                200,
                fake_timeuuid,
                'commons_page_2',
                'commonswiki',
                90,
                ['zwiki'],
                ['istype-lead-image'],
                34567,
                'YA_sect_title',
                snapshot,
                'bwiki',
                'Q7',
                1984,
            ),
        ],
        expected_schema,
    )
    # Mock needed, `format_suggestions` calls `shared.create_dataset_id`
    mocker.patch(
        'image_suggestions.shared.create_dataset_id', return_value=fake_timeuuid
    )
    suggestions_clean = slis.format_suggestions(suggestions_full)
    actual = shared.save_suggestions(suggestions_clean, output_db, snapshot, coalesce)

    assert_df_equality(actual, expected)
    shared.save_table.assert_called_with(
        actual,
        output_db,
        shared.SUGGESTIONS_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )
