#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.

from chispa import assert_df_equality

from image_suggestions import commons, shared


def test_get_commons_data(spark_session):
    wd_data = spark_session.createDataFrame(
        [
            # a) regular p18 tag
            (20, 'p18', 'Q999', 19),
            # b) 2 p373 tags for the same page_id, with values in reverse order
            (20, 'p373', 'Q888', 1000),
            (20, 'p373', 'Q777', 0),
        ],
        ['page_id', 'tag', 'item_id', 'score'],
    )
    li_data = spark_session.createDataFrame(
        [
            # c) regular lead_image tag
            (20, 'Q999', 'lead_image', 19, ['arwiki', 'gawiki']),
            # d) many lead_image tags for the same page_id, with values out of order
            (10, 'Q888', 'lead_image', 1000, ['enwiki']),
            (10, 'Q666', 'lead_image', 6, ['itwiki', 'gawiki']),
            (10, 'Q999', 'lead_image', 200, ['gawiki']),
            (10, 'Q777', 'lead_image', 11, ['itwiki']),
        ],
        # note the column order in wd_data and li_data is different, to make sure the code is robust enough
        # to deal with it
        ['page_id', 'item_id', 'tag', 'score', 'found_on'],
    )
    expected = spark_session.createDataFrame(
        [
            # d) checking order by page_id and tag, and ordering of values
            (
                'commonswiki',
                6,
                10,
                'lead_image',
                ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200'],
            ),
            # c)
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
            # a)
            ('commonswiki', 6, 20, 'p18', ['Q999|19']),
            # b)
            ('commonswiki', 6, 20, 'p373', ['Q777|0', 'Q888|1000']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    actual = commons.build_weighted_tags(wd_data, li_data)

    assert_df_equality(actual, expected, ignore_schema=True)


def test_write_weighted_tags(spark_session, mocker):
    mocker.patch('image_suggestions.shared.save_search_index_full')
    mocker.patch('image_suggestions.shared.save_table')
    tags = spark_session.createDataFrame(
        [
            (
                'commonswiki',
                6,
                10,
                'lead_image',
                ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200'],
            ),
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
            ('commonswiki', 6, 20, 'p18', ['Q999|19']),
            ('commonswiki', 6, 20, 'p373', ['Q777|0', 'Q888|1000']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    # We're not testing the delta, so just return the initial tags
    mocker.patch('image_suggestions.shared.build_search_index_delta', return_value=tags)

    hive_db = 'beez'
    snapshot = '2002-02-20'
    coalesce = 666

    # Tags row count = threshold, so save the delta
    delta_threshold = 4
    commons.write_weighted_tags(
        spark_session, tags, hive_db, snapshot, coalesce, delta_threshold
    )
    shared.save_table.assert_called_once()

    # Tags row count > threshold, so don't save the delta
    shared.save_table.reset_mock()
    delta_threshold = 1
    commons.write_weighted_tags(
        spark_session, tags, hive_db, snapshot, coalesce, delta_threshold
    )
    shared.save_table.assert_not_called()
