#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from chispa import assert_df_equality
from pyspark.sql import functions as F
from pyspark.sql.types import NullType

from image_suggestions import alis, shared, unillustratable


def test_get_unillustrated_articles(spark_session, mocker):
    short_snapshot = '2022-01'
    mocker.patch(
        'image_suggestions.shared.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (1, 'commons_page_1'),
                (2, 'commons_page_probable_icon'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.overused_images.get',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 2, 'commons_page_probable_icon'),
                ('ewiki', 2, 'commons_page_probable_icon'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.alis.load_local_images',
        return_value=spark_session.createDataFrame(
            [
                ('dwiki', 10, 'local_file_1'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_imagelinks',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'commons_page_1'),
                ('bwiki', 222, 'commons_page_probable_icon'),
                ('bwiki', 333, 'commons_page_probable_icon'),
                ('bwiki', 333, 'commons_page_1'),
                ('dwiki', 444, 'local_file_1'),
                ('ewiki', 555, 'commons_page_probable_icon'),
                ('fwiki', 666, 'commons_page_probable_icon'),
            ],
            ['wiki_db', 'article_id', 'image_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_non_commons_main_pages',
        return_value=spark_session.createDataFrame(
            [
                # linked to 'commons_page_1' so should NOT be returned
                ('awiki', 111, 'wiki_page_111'),
                # linked ONLY to an image with too many links, so should be returned
                ('bwiki', 222, 'wiki_page_222'),
                # linked to an image with too many links but also another image, so should NOT be returned
                ('bwiki', 333, 'wiki_page_333'),
                ('bwiki', 333, 'wiki_page_333'),
                # no imagelink, so should be returned
                ('cwiki', 444, 'wiki_page_444'),
                # imagelink to local_file_1, should NOT be returned
                ('dwiki', 444, 'wiki_page_444'),
                # linked to an image with too many links on this wiki, so should be returned
                ('ewiki', 555, 'wiki_page_555'),
                # linked to an image with too many links on another wiki (but not this one), so should NOT be returned
                ('fwiki', 666, 'wiki_page_666'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'wiki_page_222'),
            ('cwiki', 444, 'wiki_page_444'),
            ('ewiki', 555, 'wiki_page_555'),
        ],
        ['wiki_db', 'page_id', 'page_title'],
    )
    actual = alis.get_unillustrated_articles(spark_session, short_snapshot)
    assert_df_equality(actual, expected)


def test_get_illustratable_articles(spark_session, mocker):
    mocker.patch(
        'image_suggestions.alis.get_unillustrated_articles',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'wiki_page_111'),
                ('bwiki', 222, 'wiki_page_222'),
                ('cwiki', 333, 'wiki_page_333'),
                ('cwiki', 111, 'wiki_page_111'),
                ('cwiki', 999, 'article_with_no_item_id'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_wikidata_item_page_links',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 111, 'Q1'),
                ('bwiki', 222, 'Q2'),
                ('cwiki', 333, 'Q3'),
                ('cwiki', 111, 'Q5'),
            ],
            ['wiki_db', 'page_id', 'item_id'],
        ),
    )
    mocker.patch(
        'image_suggestions.unillustratable.get_non_illustratable_item_ids',
        return_value=spark_session.createDataFrame(
            [
                {'item_id': 'Q1'},
                {'item_id': 'Q3'},
                {'item_id': 'Q4'},
            ]
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'wiki_page_222', 'Q2'),
            ('cwiki', 111, 'wiki_page_111', 'Q5'),
        ],
        ['wiki_db', 'page_id', 'page_title', 'item_id'],
    )
    actual = alis.get_illustratable_articles(spark_session, '2022-01-01')
    assert_df_equality(actual, expected)


def test_get_illustratable_articles_with_revisions(spark_session, mocker):
    mocker.patch(
        'image_suggestions.alis.get_illustratable_articles',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 222, 'article_title_1', 'Q2'),
                ('cwiki', 111, 'article_title_2', 'Q5'),
                # article without latest revision (shouldn't happen, but test just in case)
                ('ewiki', 999, 'article_title_3', 'Q999'),
            ],
            ['wiki_db', 'page_id', 'page_title', 'item_id'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_latest_revisions',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 222, 1234567),
                ('cwiki', 111, 2345678),
                # article that is not illustrable
                ('dwiki', 111, 9876543),
            ],
            ['wiki_db', 'page_id', 'rev_id'],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('bwiki', 222, 'article_title_1', 'Q2', 1234567),
            ('cwiki', 111, 'article_title_2', 'Q5', 2345678),
        ],
        ['wiki_db', 'page_id', 'page_title', 'item_id', 'rev_id'],
    )
    actual = alis.get_illustratable_articles_with_revisions(spark_session, '2022-01-01')
    assert_df_equality(actual, expected)


def test_generate_suggestions(spark_session, mocker):
    fake_timeuuid = 'xyz'
    snapshot = '2022-01-01'
    coalesce = 2
    mocker.patch('image_suggestions.shared.save_suggestions')
    mocker.patch('image_suggestions.shared.save_title_cache')
    mocker.patch('image_suggestions.shared.save_instanceof_cache')
    mocker.patch(
        'image_suggestions.shared.create_dataset_id', return_value=fake_timeuuid
    )
    mocker.patch(
        'image_suggestions.entity_images.get',
        # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
        return_value=spark_session.createDataFrame(
            [
                (
                    'Q1',
                    'commons_page_1',
                    90,
                    ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                    ['pwiki', 'qwiki'],
                ),
                # suggestion without a corresponding article in a wiki
                (
                    'Q100',
                    'commons_page_100',
                    80,
                    ['istype-depicts', 'istype-lead-image'],
                    ['xwiki', 'ywiki'],
                ),
            ],
            ['item_id', 'page_title', 'confidence', 'kind', 'found_on'],
        )
        .union(
            spark_session.createDataFrame(
                [
                    (
                        'Q2',
                        'commons_page_2',
                        80,
                        ['istype-commons-category', 'istype-depicts'],
                    ),
                    (
                        'Q3',
                        'commons_page_3',
                        80,
                        ['istype-commons-category', 'istype-depicts'],
                    ),
                    (
                        'Q4',
                        'commons_page_4',
                        80,
                        ['istype-commons-category', 'istype-depicts'],
                    ),
                    (
                        'Q5',
                        'commons_page_5',
                        80,
                        ['istype-commons-category', 'istype-depicts'],
                    ),
                    (
                        'Q6',
                        'commons_page_6',
                        80,
                        ['istype-commons-category', 'istype-depicts'],
                    ),
                ],
                ['item_id', 'page_title', 'confidence', 'kind'],
            ).withColumn('found_on', F.lit(None).cast(NullType()))
        )
        .orderBy('item_id', 'page_title'),
    )
    mocker.patch(
        'image_suggestions.alis.load_suggestions_with_feedback',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 300, 'commons_page_2'),
                # feedback for something that is not a suggestion, ought to be ignored
                ('cwiki', 111, 'commons_page_x'),
            ],
            ['wiki', 'page_id', 'filename'],
        ),
    )
    mocker.patch(
        'image_suggestions.unillustratable.get_images_in_placeholder_categories',
        return_value=spark_session.createDataFrame(
            [
                (123, 'Some_category_title', 'file', 'commons_page_3'),
                # unrelated image
                (234, 'Some_category_title', 'file', 'commons_page_y'),
            ],
            ['cl_from', 'cl_to', 'cl_type', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.overused_images.get',
        return_value=spark_session.createDataFrame(
            [
                ('bwiki', 345, 'commons_page_4'),
                # unrelated image
                ('bwiki', 456, 'commons_page_z'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.unillustratable.get_allowed_suffixes_regex',
        return_value=unillustratable.get_allowed_suffixes_regex(
            (
                '_page_1',
                '_page_2',
                '_page_3',
                '_page_4',
                '_page_6',
            )
        ),
    )
    mocker.patch(
        'image_suggestions.unillustratable.get_disallowed_substrings_regex',
        return_value=unillustratable.get_disallowed_substrings_regex(('_page_6',)),
    )
    mocker.patch(
        'image_suggestions.alis.get_illustratable_articles_with_revisions',
        return_value=spark_session.createDataFrame(
            [
                ('awiki', 100, 'awiki_page_title_100', 'Q1', 12345),
                ('awiki', 101, 'awiki_page_title_101', 'Q2', 23456),
                ('bwiki', 200, 'bwiki_page_title_200', 'Q2', 34567),
                # article without suggestion
                ('cwiki', 111, 'cwiki_page_title_111', 'Q3', 45678),
                # the suggestion for this article has been rejected or accepted already, should be absent from results
                ('bwiki', 300, 'bwiki_page_title_300', 'Q2', 56789),
                # suggestions filtered out for other reasons
                ('bwiki', 400, 'bwiki_page_title_400', 'Q3', 67890),
                ('bwiki', 500, 'bwiki_page_title_500', 'Q4', 78901),
                ('bwiki', 600, 'bwiki_page_title_600', 'Q5', 89012),
                ('bwiki', 700, 'bwiki_page_title_700', 'Q6', 90123),
            ],
            ['wiki_db', 'page_id', 'page_title', 'item_id', 'rev_id'],
        ),
    )
    # separating out the expected values with a null 'found_on' field to make the data frame easier to construct
    expected_raw = spark_session.createDataFrame(
        [
            (
                'awiki',
                100,
                'awiki_page_title_100',
                'Q1',
                fake_timeuuid,
                'commons_page_1',
                'commonswiki',
                90,
                ['pwiki', 'qwiki'],
                ['istype-depicts', 'istype-lead-image', 'istype-wikidata-image'],
                12345,
            ),
        ],
        [
            'wiki',
            'page_id',
            'page_title',
            'item_id',
            'id',
            'image',
            'origin_wiki',
            'confidence',
            'found_on',
            'kind',
            'page_rev',
        ],
    ).union(
        spark_session.createDataFrame(
            [
                (
                    'awiki',
                    101,
                    'awiki_page_title_101',
                    'Q2',
                    fake_timeuuid,
                    'commons_page_2',
                    'commonswiki',
                    80,
                    ['istype-commons-category', 'istype-depicts'],
                    23456,
                ),
                (
                    'bwiki',
                    200,
                    'bwiki_page_title_200',
                    'Q2',
                    fake_timeuuid,
                    'commons_page_2',
                    'commonswiki',
                    80,
                    ['istype-commons-category', 'istype-depicts'],
                    34567,
                ),
            ],
            [
                'wiki',
                'page_id',
                'page_title',
                'item_id',
                'id',
                'image',
                'origin_wiki',
                'confidence',
                'kind',
                'page_rev',
            ],
        )
        .withColumn('found_on', F.lit(None).cast(NullType()))
        .select(
            'wiki',
            'page_id',
            'page_title',
            'item_id',
            'id',
            'image',
            'origin_wiki',
            'confidence',
            'found_on',
            'kind',
            'page_rev',
        )
    )
    expected = alis.prepare_for_saving(expected_raw)
    actual = alis.generate_suggestions(spark_session, 'test', snapshot, 666, coalesce)

    assert_df_equality(actual, expected, ignore_schema=True)
    shared.save_suggestions.assert_called_with(actual, 'test', snapshot, coalesce)
    shared.save_title_cache.assert_called_with(actual, 'test', snapshot, coalesce)
    shared.save_instanceof_cache.assert_called_with(
        spark_session, actual, 'test', snapshot, coalesce
    )
