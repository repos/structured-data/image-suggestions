#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.

import pytest

from chispa import assert_df_equality

from image_suggestions import shared, wiki_indices


def test_build_exists_tags(spark_session):
    suggestions = spark_session.createDataFrame(
        [
            ('aawiki', 111),
            ('bbwiki', 222),
            ('ccwiki', 333),
        ],
        ['wiki', 'page_id'],
    )

    with pytest.raises(ValueError):
        wiki_indices.build_exists_tags('baddy', suggestions)

    alis_expected = spark_session.createDataFrame(
        [
            ('aawiki', 0, 111, 'recommendation.image', ['exists|1']),
            ('bbwiki', 0, 222, 'recommendation.image', ['exists|1']),
            ('ccwiki', 0, 333, 'recommendation.image', ['exists|1']),
        ],
        [
            shared.WIKIID_COLNAME,
            shared.PAGE_NAMESPACE_COLNAME,
            'page_id',
            shared.TAG_COLNAME,
            shared.VALUES_COLNAME,
        ],
    )
    alis_actual = wiki_indices.build_exists_tags('alis', suggestions)
    assert_df_equality(alis_actual, alis_expected, ignore_schema=True)

    slis_expected = spark_session.createDataFrame(
        [
            ('aawiki', 0, 111, 'recommendation.image_section', ['exists|1']),
            ('bbwiki', 0, 222, 'recommendation.image_section', ['exists|1']),
            ('ccwiki', 0, 333, 'recommendation.image_section', ['exists|1']),
        ],
        [
            shared.WIKIID_COLNAME,
            shared.PAGE_NAMESPACE_COLNAME,
            'page_id',
            shared.TAG_COLNAME,
            shared.VALUES_COLNAME,
        ],
    )
    slis_actual = wiki_indices.build_exists_tags('slis', suggestions)
    assert_df_equality(slis_actual, slis_expected, ignore_schema=True)
