#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Test dataframes created with plain lists of column names yield
# implicit schema inferences. We're not testing the schema anyway,
# so let assertions ignore it as needed.

from chispa import assert_df_equality
from pyspark.sql import types as T

from image_suggestions import alignment_slis


def test_get_section_alignment_suggestions_with_page_data(spark_session, mocker):
    weekly_snapshot = '2023-02-04'
    mocker.patch(
        'image_suggestions.alignment_slis.read_parquet',
        return_value=spark_session.createDataFrame(
            [
                (
                    'Q1234',
                    111,
                    'Aa_article_title',
                    666,
                    'Section_heading_a',
                    [],
                    'aawiki',
                ),
                ('Q2345', 222, 'Bb_article_title', 1984, 'Bb_heading', [], 'bbwiki'),
            ],
            # `recommended_images` has a complex type that can't be inferred via
            # a plain list of column names, so pass a full schema.
            T.StructType(
                [
                    T.StructField('item_id', T.StringType(), False),
                    T.StructField('target_id', T.IntegerType(), False),
                    T.StructField('target_title', T.StringType(), False),
                    T.StructField('target_index', T.IntegerType(), False),
                    T.StructField('target_heading', T.StringType(), False),
                    T.StructField(
                        'recommended_images',
                        T.ArrayType(
                            T.MapType(T.StringType(), T.ArrayType(T.StringType()))
                        ),
                        False,
                    ),
                    T.StructField('target_wiki_db', T.StringType(), False),
                ]
            ),
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_non_commons_main_pages',
        return_value=spark_session.createDataFrame(
            [
                ('aawiki', 111, 'Aa_article_title'),
                ('bbwiki', 222, 'Bb_article_title'),
                ('ccwiki', 888, 'Cc_article_title'),
            ],
            ['wiki_db', 'page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.shared.load_latest_revisions',
        return_value=spark_session.createDataFrame(
            [
                ('aawiki', 111, 11111),
                ('bbwiki', 222, 22222),
                ('ccwiki', 999, 99999),
            ],
            ['wiki_db', 'page_id', 'rev_id'],
        ),
    )
    actual = alignment_slis.get_section_alignment_suggestions_with_page_data(
        spark_session, weekly_snapshot, '/path/to/parquet'
    )
    expected = spark_session.createDataFrame(
        [
            (
                'Q1234',
                11111,
                111,
                'Aa_article_title',
                666,
                'Section_heading_a',
                [],
                'aawiki',
            ),
            ('Q2345', 22222, 222, 'Bb_article_title', 1984, 'Bb_heading', [], 'bbwiki'),
        ],
        T.StructType(
            [
                T.StructField('item_id', T.StringType(), False),
                # These 2 columns are nullable: they originally come from
                # `wmf_raw.mediawiki_revision`, where all columns are nullable
                T.StructField('target_page_rev_id', T.LongType(), True),
                T.StructField('target_page_id', T.LongType(), True),
                #
                T.StructField('target_page_title', T.StringType(), False),
                T.StructField('target_index', T.IntegerType(), False),
                T.StructField('target_heading', T.StringType(), False),
                T.StructField(
                    'recommended_images',
                    T.ArrayType(T.MapType(T.StringType(), T.ArrayType(T.StringType()))),
                    False,
                ),
                T.StructField('target_wiki_db', T.StringType(), False),
            ]
        ),
    )

    assert_df_equality(actual, expected, ignore_schema=True)


def test_get(spark_session, mocker):
    weekly_snapshot = '2023-02-02'
    mocker.patch(
        'image_suggestions.shared.load_commons_images',
        return_value=spark_session.createDataFrame(
            [
                (1, 'Image_1.jpg'),
                (2, 'Image_2.jpg'),
                (2, 'Image_3.jpg'),
            ],
            ['page_id', 'page_title'],
        ),
    )
    mocker.patch(
        'image_suggestions.alignment_slis.get_expanded_section_alignment_suggestions',
        return_value=spark_session.createDataFrame(
            [
                (
                    'awiki',
                    123456,
                    789,
                    'Page_title_1',
                    666,
                    'Section_title_1',
                    'Q111',
                    'Image_1.jpg',
                    'ptwiki',
                ),
                (
                    'awiki',
                    123456,
                    789,
                    'Page_title_1',
                    666,
                    'Section_title_1',
                    'Q111',
                    'Image_1.jpg',
                    'enwiki',
                ),
                (
                    'bwiki',
                    456789,
                    123,
                    'Page_title_2',
                    77,
                    'Section_title_2',
                    'Q222',
                    'Image_2.jpg',
                    'eswiki',
                ),
                (
                    'cwiki',
                    456789,
                    123,
                    'Page_title_4',
                    77,
                    'Section_title_4',
                    'Q444',
                    'Non-Commons_image_4.jpg',
                    'nlwiki',
                ),
            ],
            [
                'wiki_db',
                'target_page_rev_id',
                'target_page_id',
                'target_page_title',
                'target_section_index',
                'target_section_heading',
                'item_id',
                'suggested_image',
                'origin_wiki',
            ],
        ),
    )
    actual = alignment_slis.get(spark_session, weekly_snapshot, '/test/path')
    expected = spark_session.createDataFrame(
        [
            (
                'awiki',
                123456,
                789,
                'Page_title_1',
                666,
                'Section_title_1',
                'Image_1.jpg',
                'Q111',
                'istype-section-alignment',
                ['ptwiki', 'enwiki'],
                80,
            ),
            (
                'bwiki',
                456789,
                123,
                'Page_title_2',
                77,
                'Section_title_2',
                'Image_2.jpg',
                'Q222',
                'istype-section-alignment',
                ['eswiki'],
                80,
            ),
        ],
        [
            'wiki_db',
            'target_page_rev_id',
            'target_page_id',
            'target_page_title',
            'target_section_index',
            'target_section_heading',
            'suggested_image',
            'target_qid',
            'kind',
            'origin_wikis',
            'confidence',
        ],
    )

    assert_df_equality(actual, expected, ignore_schema=True)
