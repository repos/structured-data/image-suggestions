Search indices
==============

We generate a dataset that enables queries against Wikimedia Foundation's
`search indices <https://www.mediawiki.org/wiki/Help:CirrusSearch>`_.
It serves two purposes:

- inject :ref:`sources` into Commons
- deliver all available image suggestions to Wikipedias


Commons
-------

.. automodule:: image_suggestions.commonswiki_file
   :members:

Wikis
-----

.. automodule:: image_suggestions.search_indices
   :members:
