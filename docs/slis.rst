.. _slis:

SLIS: section-level image suggestions
=====================================

.. automodule:: image_suggestions.section_image_suggestions


Section topics suggestions
--------------------------

.. automodule:: image_suggestions.section_topics_images


Section alignment suggestions
-----------------------------

.. automodule:: image_suggestions.section_alignment_images
