-- Usage:
--     spark3-sql -f alter_image_suggestions_suggestions_20230209.hql \
--                --database analytics_platform_eng


ALTER TABLE `image_suggestions_suggestions` ADD COLUMNS (
    `section_heading` string
);
