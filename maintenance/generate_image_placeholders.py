#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# NOTE Run me from my parent directory

import argparse
import sys

import requests


# This URL returns a list of image placeholder categories
PETSCAN_URL = 'https://petscan.wmflabs.org/?psid=18699732&format=json'
OUTPUT = 'image_placeholders'


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Generate a HDFS dataset of placeholder Commons images'
    )
    parser.add_argument('snapshot', metavar='YYYY-MM', help='monthly snapshot date')
    parser.add_argument(
        '-o',
        '--output',
        metavar='/hdfs_path/to/parquet',
        default=OUTPUT,
        help=f'HDFS path to output parquet. Default: "{OUTPUT}" in the current user home',
    )

    return parser.parse_args()


def main():
    args = parse_args()
    snapshot = args.snapshot
    output = args.output

    try:
        response = requests.get(PETSCAN_URL)
    except:
        print(
            'Looks like Petscan is not working: '
            'check https://petscan.wmflabs.org/ and try again'
        )
        sys.exit(1)

    if not response.ok:
        print(
            f'Response by Petscan not OK, ' 'got HTTP {response.status_code}. Try again'
        )
        sys.exit(2)

    try:
        json_response = response.json()
    except:
        print('Got no JSON from Petscan, try again')
        sys.exit(3)

    try:
        cats = json_response['*'][0]['a']['*']
    except KeyError:
        print(
            "Couldn't get the data I need, maybe the keys changed? "
            'Check out the raw JSON:',
            cats,
            separator='\n',
        )
        sys.exit(4)

    quoted_cats = []
    for cat in cats:
        try:
            title = cat['title']
        except KeyError:
            print('Skipping category with no page title:', cat)
            continue
        quoted_cats.append(f"'{title}'")

    # Uncomment for prod
    # from pyspark.sql import SparkSession
    # spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    from wmfdata.spark import create_session

    spark = create_session(app_name='generate-image-placeholders', ship_python_env=True)

    links = spark.sql(
        f"""SELECT cl_from, cl_to, cl_type
        FROM wmf_raw.mediawiki_categorylinks
        WHERE snapshot='{snapshot}'
        AND wiki_db='commonswiki' AND cl_to IN ({','.join(quoted_cats)})
        """
    )
    pages = spark.sql(
        f"""SELECT page_id, page_title
        FROM wmf_raw.mediawiki_page
        WHERE snapshot='{snapshot}'
        AND wiki_db='commonswiki' AND page_namespace=6
        """
    )
    dataset = links.join(pages.withColumnRenamed('page_id', 'cl_from'), on=['cl_from'])
    dataset.write.parquet(output)

    spark.stop()


if __name__ == '__main__':
    main()
