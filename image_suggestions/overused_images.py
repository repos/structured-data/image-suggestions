#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""When images are overused, they are probably placeholders or icons, thus being unsuitable for suggestions.
This module computes overusage.
"""

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from image_suggestions import queries, shared


def load_wiki_sizes(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load wikis with their article page counts through the
    :const:`image_suggestions.queries.wiki_sizes` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - size (bigint) - total article pages
    """
    return spark.sql(queries.wiki_sizes.format(monthly_snapshot))


def get_link_thresholds_per_wiki(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:
    """Compute per-wiki thresholds that delimit overlinkage.

    If the amount of links from articles to a given image is above a threshold,
    then the image is considered as overused in the corresponding wiki.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - size (bigint) - total article pages
        - threshold (double) - threshold that delimits too many links
    """
    wiki_sizes = load_wiki_sizes(spark, monthly_snapshot)

    return wiki_sizes.withColumn(
        'threshold',
        F.ceil(
            F.when(
                wiki_sizes.size >= 50000, (F.log10(wiki_sizes.size / 50000) + 1) * 15
            )
            .when(wiki_sizes.size > 12500, (wiki_sizes.size / 50000) * 15)
            .otherwise(4)
        ),
    )


def get(spark: SparkSession, monthly_snapshot: str) -> DataFrame:
    """Identify overused images.

    If an image is overused, then it's probably a placeholder or an icon
    and shouldn't be suggested.

    .. note::

        Data is significantly skewed:
        some partitions have 2 orders of magnitude more rows than others.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - page_id (bigint) - Commons page ID
        - page_title (string) - Commons page title, in original case and underscored
    """
    thresholds = get_link_thresholds_per_wiki(spark, monthly_snapshot)
    images = shared.load_commons_images(spark, monthly_snapshot)
    imagelinks = shared.load_imagelinks(spark, monthly_snapshot)

    return (
        images.join(
            imagelinks, on=[images.page_title == imagelinks.image_title], how='inner'
        )
        .groupBy(imagelinks.wiki_db, images.page_id, images.page_title)
        .agg(F.count(imagelinks.article_id).alias('link_count'))
        .join(thresholds, on=['wiki_db'], how='inner')
        .where('link_count > threshold')
        .select('wiki_db', 'page_id', 'page_title')
    )
