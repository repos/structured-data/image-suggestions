#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This algorithm builds on top of the :mod:`section_topics.pipeline` and aims at
constructing a visual representation of wikilinks available in Wikipedia article sections.
To achieve so, it follows two kinds of paths that connect a given wikilink to a Commons image, namely:

- wikilink → Wikidata QID → Wikidata image property → Commons image
- wikilink → Wikipedia article's lead image

The former path consumes `image <https://www.wikidata.org/wiki/Property:P18>`_ and
`Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata claims.
Note that we explored the use of additional ones with no success,
see `here <https://phabricator.wikimedia.org/T311832>`_ for more details.
"""

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from image_suggestions import entity_images


def get(
    spark: SparkSession, hive_db: str, weekly_snapshot: str, section_topics_parquet: str
) -> DataFrame:
    """Gather image suggestions based on section topics.

    Build the topic's visual representation from :ref:`sources`.
    Exclude Commons depicts statements: the dataset is skewed,
    and leads to a never-ending pipeline execution.
    See `this read <https://towardsdatascience.com/data-skew-in-pyspark-783d529a9dd7>`_ and
    `this comment <https://stackoverflow.com/a/48469892/10719765>`_
    for more details on PySpark's data skew.

    Ensure that an image holds the same relationship
    with both a topic and the page where the topic comes from:
    match the image Wikidata QID against the topic and page QIDs.

    Compute an image suggestion confidence score between 0 and 100
    by combining topic and page image scores as output by :func:`image_suggestions.entity_images.get`
    with a topic relevance constant of ``90`` based on manual evaluation:

    ::

        round( 90 x (topic image score : 100) x (page image score : 100) )

    :param spark: an active Spark session
    :param hive_db: a Data Lake's `Hive <https://hive.apache.org/>`_ database name
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :param section_topics_parquet: a HDFS path to a parquet generated by :mod:`section_topics.pipeline`
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - target_page_rev_id (bigint): page revision ID
        - target_page_id (bigint) - page ID
        - target_page_title (string) - page title, in original case and underscored
        - target_section_index (int) - section numerical index, starts from 1
        - target_section_heading (string) - page section, in URL anchor format.
          More details in :func:`section_topics.pipeline.wikitext_headings_to_anchors`
        - suggested_image (string) - Commons page title, in original case and underscored
        - target_qid (string) - page Wikidata QID
        - topic_qid (string) - topic Wikidata QID
        - kind (string) - ``istype-section-topics``
        - confidence (int) - suggestion confidence score
    """
    topics = get_section_topics(spark, section_topics_parquet, [0])
    image_links = entity_images.get(
        spark,
        hive_db,
        weekly_snapshot,
        include_sdc=False,  # Exclude depicts statements
    )

    return (
        topics
        # Match suggested images against both the topic and the page QIDs
        .join(
            image_links.alias('topic_images'),
            on=[F.col('topic_images.item_id') == topics.topic_qid],
            how='inner',
        )
        .join(
            image_links.alias('page_images'),
            on=[
                F.col('page_images.item_id') == topics.page_qid,
                F.col('page_images.page_title') == F.col('topic_images.page_title'),
            ],
            how='inner',
        )
        .select(
            'wiki_db',
            topics.revision_id.alias('target_page_rev_id'),
            topics.page_id.alias('target_page_id'),
            topics.page_title.alias('target_page_title'),
            topics.section_index.alias('target_section_index'),
            topics.section_title.alias('target_section_heading'),
            F.col('topic_images.page_title').alias('suggested_image'),
            topics.page_qid.alias('target_qid'),
            'topic_qid',
            F.lit('istype-section-topics').alias('kind'),
            # Assign a confidence score (within a 0-100 range) by combining the confidence of
            # an image being a good match for *both* the section topic (topic_images.confidence) and
            # the page (page_images.confidence), with the likelihood of the section topic being relevant
            # (currently just a hardcoded number based on early manual evaluation data).
            F.round(
                F.lit(90)
                * F.col('topic_images.confidence')
                / F.lit(100)
                * F.col('page_images.confidence')
                / F.lit(100)
            ).alias('confidence'),
        )
        .distinct()
    )


def read_section_topics_parquet(
    spark: SparkSession, section_topics_parquet: str
) -> DataFrame:  # pragma: no cover
    """
    Returns dataframe containing all section topics data,
    as generated by section_topics/pipeline.py in structured-data/section-topics repo.

    :param spark: Spark session
    :param section_topics_parquet: Path to parquet generated by section_topics/pipeline.py
    :returns Dataframe with the following columns:
        snapshot (string)
        wiki_db (string): Wiki dbname
        page_namespace (int): Wiki page namespace
        revision_id (bigint): Wiki page revision id
        page_qid (string): Wikidata entity id associated with page
        page_id (bigint): Wiki page id
        page_title (string): Wiki page title, in original case & underscored
        section_index (int): Wiki page section numerical index, 0 is the lead section
        section_title (string): Wiki page section, in id attribute format
        topic_qid (string): Wikidata entity id associated with section
        topic_title (string)
        topic_score (double)
    """

    return spark.read.parquet(section_topics_parquet)


def get_section_topics(
    spark: SparkSession, section_topics_parquet: str, exclude_section_indexes: list
) -> DataFrame:
    """
    Returns dataframe containing unique section topics.

    :param spark: Spark session
    :param section_topics_parquet: Path to parquet generated by section_topics/pipeline.py
    :param exclude_section_indexes: List of section indexes to exclude
    :type exclude_section_indexes: list[int]
    :returns Dataframe with the following columns:
        wiki_db (string): Wiki dbname
        revision_id (bigint): Wiki page revision id
        page_id (bigint): Wiki page id
        page_title (string): Wiki page title, in original case & underscored
        section_index (int): Wiki page section numerical index
        section_title (string): Wiki page section, in id attribute format
        page_qid (string): Wikidata entity id associated with page
        topic_qid (string): Wikidata entity id associated with section
    """

    section_topics = read_section_topics_parquet(spark, section_topics_parquet)

    return (
        section_topics.where(
            section_topics.section_index.isin(exclude_section_indexes) == False
        )
        .select(
            'wiki_db',
            'revision_id',
            'page_id',
            'page_title',
            'section_index',
            'section_title',
            'page_qid',
            'topic_qid',
        )
        .distinct()
    )
