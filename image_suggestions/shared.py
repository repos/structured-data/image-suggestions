#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A set of constants and functions shared across the code base."""

import uuid

from argparse import ArgumentParser
from datetime import datetime, timedelta
from typing import Tuple

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType, StringType

from image_suggestions import queries


# Spark settings for development on stat boxes.
# They should mirror ALIS' production DAG properties:
# https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/alis_dag.py
DEV_SPARK_SETTINGS = {
    'spark.driver.cores': 2,
    'spark.driver.memory': '12g',
    'spark.executor.cores': 4,
    'spark.executor.memory': '20g',
    'spark.dynamicAllocation.maxExecutors': 64,
    'spark.sql.shuffle.partitions': 256,
    'spark.sql.sources.partitionOverwriteMode': 'dynamic',
}

WIKIID_COLNAME = 'wikiid'
PAGE_NAMESPACE_COLNAME = 'page_namespace'
TAG_COLNAME = 'tag'
SCORE_COLNAME = 'score'
FOUND_ON_COLNAME = 'found_on'
CONFIDENCE_COLNAME = 'confidence'
KIND_COLNAME = 'kind'
DATASET_ID_COLNAME = 'id'
ORIGIN_WIKI_COLNAME = 'origin_wiki'
VALUES_COLNAME = 'values'

COMMONSWIKI_VALUE = 'commonswiki'

#: Hive table of image suggestions,
#: holding both article-level and section-level ones.
#: To be consumed by Wikimedia Foundation's
#: `Cassandra <https://cassandra.apache.org/>`_ instance,
#: and exposed through the
#: `Data Gateway Service <https://www.mediawiki.org/wiki/Platform_Engineering_Team/Data_Value_Stream/Data_Gateway>`_.
SUGGESTIONS_TABLE = 'image_suggestions_suggestions'
#: Hive table of Wikipedia article candidates for suggestions.
#: To be consumed by Cassandra and exposed through the Data Gateway Service.
TITLE_CACHE_TABLE = 'image_suggestions_title_cache'
#: Hive table of Wikipedia articles that are instances of Wikidata QIDs.
#: To be consumed by Cassandra and exposed through the Data Gateway Service.
INSTANCEOF_CACHE_TABLE = 'image_suggestions_instanceof_cache'
#: Commons search index's weighted tags Hive table, full dataset.
SEARCH_INDEX_FULL_TABLE = 'image_suggestions_search_index_full'
#: Commons search index's weighted tags Hive table, diff/delta dataset.
#: To be consumed by Wikimedia Foundation's
#: `Elasticsearch <https://www.elastic.co/elasticsearch/>`_ instance
#: through `CirrusSearch <https://www.mediawiki.org/wiki/Extension:CirrusSearch>`_.
SEARCH_INDEX_DELTA_TABLE = 'image_suggestions_search_index_delta'
# NOTE the following tables are in `queries.py` to avoid
#      a circular import that breaks Spark
#: Hive table of `image <https://www.wikidata.org/wiki/Property:P18>`_ and
#: `Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata
#: `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_.
WIKIDATA_TABLE = queries.WIKIDATA_TABLE
#: Hive table of Wikipedia article lead images.
LEAD_IMAGES_TABLE = queries.LEAD_IMAGES_TABLE

#: Search index weighted tags relevant to image suggestions.
P18_TAG = 'image.linked.from.wikidata.p18'
P373_TAG = 'image.linked.from.wikidata.p373'
LEAD_IMAGE_TAG = 'image.linked.from.wikipedia.lead_image'
RECOMMENDATION_IMAGE_TAG = 'recommendation.image'
RECOMMENDATION_IMAGE_SECTION_TAG = 'recommendation.image_section'
RELEVANT_TAGS = (
    P18_TAG,
    P373_TAG,
    LEAD_IMAGE_TAG,
    RECOMMENDATION_IMAGE_TAG,
    RECOMMENDATION_IMAGE_SECTION_TAG,
)


def get_monthly_snapshot(snapshot: str) -> str:
    """Get the most recent monthly snapshot given a weekly one.

    .. note::

        This function has the same behavior as :func:`section_topics.pipeline.get_monthly_snapshot`,
        although it doesn't contain identical code.

    A snapshot date is the **beginning** of the snapshot interval. For instance:

    - ``2022-05-16`` covers until ``2022-05-22`` (at 23:59:59).
      May is not over, so the May monthly snapshot is not available yet.
      Hence return end of **April**, i.e., ``2022-04``
    - ``2022-05-30`` covers until ``2022-06-05``.
      May is over, so the May monthly snapshot is available.
      Hence return end of **May**, i.e., ``2022-05``

    :param snapshot: a ``YYYY-MM-DD`` date
    :raises ValueError: if the passed date has an invalid format
    :return: the relevant monthly snapshot
    """
    input_format = '%Y-%m-%d'  # YYYY-MM-DD
    output_format = '%Y-%m'  # YYYY-MM

    try:
        weekly_snapshot_date = datetime.strptime(snapshot, input_format)
        end_of_week = (
            weekly_snapshot_date
            - timedelta(days=weekly_snapshot_date.weekday())
            + timedelta(days=6)
        )
        previous_month = end_of_week.replace(day=1) - timedelta(days=1)
    except ValueError:
        # Just a more eloquent error message
        raise ValueError(f'Invalid snapshot, must be YYYY-MM-DD: {snapshot}')

    return previous_month.strftime(output_format)


def get_cirrus_index_snapshot(snapshot: str) -> str:
    """Get the relevant snapshot of the production Cirrus search index
    weighted tags Hive table, see :const:`queries.cirrus_index_tags`.
    That snapshot is **1 day before** our current one.

    :param snapshot: a ``YYYY-MM-DD`` date
    :raises ValueError: if the passed date has an invalid format
    :return: the relevant Cirrus search index snapshot
    """
    input_format = '%Y-%m-%d'  # YYYY-MM-DD
    output_format = '%Y%m%d'  # YYYYMMDD

    try:
        input_date = datetime.strptime(snapshot, input_format)
        output_date = input_date - timedelta(days=1)
    except ValueError:
        # Just a more eloquent error message
        raise ValueError(f'Invalid snapshot, must be YYYY-MM-DD: {snapshot}')

    return output_date.strftime(output_format)


def load_wikidata(
    spark_session: SparkSession, hive_db: str, weekly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load `image <https://www.wikidata.org/wiki/Property:P18>`_ and
    `Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata
    `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_
    as output by :func:`image_suggestions.commonswiki_file.gather_wikidata_data`.

    :param spark_session: an active Spark session
    :param hive_db: a Data Lake's `Hive <https://hive.apache.org/>`_ database name
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of:

        - item_id (string) - Wikidata `QID <https://www.wikidata.org/wiki/WD:GLOSS#QID>`_
        - page_id (bigint) - Commons image page ID
        - tag (string) - either ``image.linked.from.wikidata.p18`` or
          ``image.linked.from.wikidata.p373``
        - score (int) - confidence score. More details
          `here <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline#Confidence_score>`_
        - snapshot (string) - ``YYYY-MM-DD`` date, same as the passed one
    """
    return spark_session.sql(queries.wikidata.format(hive_db, weekly_snapshot))


def load_non_commons_main_pages(
    spark_session: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load `article <https://en.wikipedia.org/wiki/WP:NS0>`_ pages of all wikis but Commons
    through the :const:`image_suggestions.queries.non_commons_main_pages` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - page_id (bigint) - article page ID
        - page_title (string) - article page title
    """
    return spark_session.sql(queries.non_commons_main_pages.format(monthly_snapshot))


def load_latest_revisions(
    spark_session: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load page IDs with their latest revisions through the
    :const:`image_suggestions.queries.latest_revisions` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - page_id (bigint) - page ID
        - rev_id (bigint) - latest revision ID
    """
    return spark_session.sql(queries.latest_revisions.format(monthly_snapshot))


def load_commons_images(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load Commons file page IDs and titles through the
    :const:`image_suggestions.queries.commons_file_pages` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - page_id (bigint) - file page ID
        - page_title (string) - file page title
    """
    return spark.sql(queries.commons_file_pages.format(monthly_snapshot))


def load_imagelinks(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load image file names linked to article pages of all wikis but Commons through the
    :const:`image_suggestions.queries.imagelinks` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - article_id (bigint) - article page ID
        - image_title (bigint) - image file name, in original case and underscored
    """
    return spark.sql(queries.imagelinks.format(monthly_snapshot))


def load_wikidata_items_with_p31(
    spark: SparkSession, snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load `instance-of <https://www.wikidata.org/wiki/Property:P31>`_
    Wikidata claims through the
    :const:`image_suggestions.queries.wikidata_items_with_P31` Data Lake query.

    :param spark: an active Spark session
    :param snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of:

        - wiki_db (string) - wiki project
        - page_id (bigint) - page ID
        - rev_id (bigint) - latest revision ID
    """
    return spark.sql(queries.wikidata_items_with_P31.format(snapshot))


def load_cirrus_index_tags(
    spark: SparkSession, snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load production Cirrus search index weighted tags through the
    :const:`image_suggestions.queries.cirrus_index_tags` Data Lake query.

    .. note::

        ``snapshot`` is different from the usual value,
        due to a different DB & table being queried:

        - it must be a ``YYYYMMDD`` date, not `YYYY-MM-DD``
        - it's **1 day before** the usual weekly snapshot we pass

    :param spark: an active Spark session
    :param snapshot: a ``YYYYMMDD`` date
    :return: the dataframe of:

        - wiki (string) - wiki project
        - namespace (bigint) - page namespace
        - page_id (bigint) - page ID
        - weighted_tags (array<string>) - array of weighted tags
    """
    return spark.sql(queries.cirrus_index_tags.format(snapshot))


def prepare_cirrus_index_tags(
    tags_df: DataFrame, relevant_tags: Tuple[str, ...] = RELEVANT_TAGS
) -> DataFrame:
    """Convert and filter a Cirrus search index dataset of weighted tags
    to :func:`compute_search_index_delta`'s expected input.

    Keep only relevant tags.

    :param tags_df: a dataframe of Cirrus search index weighted tags
    :param relevant_tags: a tuple of tag names to keep
    :return: the dataframe suitable for the ``previous`` argument
      of :func:`compute_search_index_delta`
    """
    return (
        tags_df.withColumn('explode', F.explode('weighted_tags'))
        .withColumn('split', F.split('explode', '/'))
        .withColumn(TAG_COLNAME, F.element_at('split', 1))
        .where(F.col(TAG_COLNAME).isin(*relevant_tags))
        .withColumn('value', F.element_at('split', 2))
        .groupBy('wiki', 'namespace', 'page_id', TAG_COLNAME)
        .agg(F.collect_list('value').alias(VALUES_COLNAME))
        .withColumnRenamed('wiki', WIKIID_COLNAME)
        .withColumnRenamed('namespace', PAGE_NAMESPACE_COLNAME)
    )


def compute_search_index_delta(
    previous: DataFrame, current: DataFrame, target_tags: list
) -> DataFrame:
    """Compute the difference between a previous and the current state of
    a search index's weighted tags dataset.

    The previous state must come from a production Cirrus search index snapshot,
    as output by :func:`prepare_cirrus_index_tags`.
    A required list of target tags is considered.
    The current state is output by either
    :mod:`image_suggestions.commonswiki_file` or :mod:`image_suggestions.search_indices`.

    Tag values are lexicographically sorted before the computation:
    this is only relevant to the Commons' search index,
    since other wikis have single boolean values.

    If a page doesn't contain tags anymore, then emit a ``__DELETE_GROUPING__`` value.

    See also `this task <https://phabricator.wikimedia.org/T302095>`_.

    :param previous: a dataframe of the previous state
    :param current: a dataframe of the current state
    :param target_tags: a list of tags to consider for the computation.
      Must not be empty
    :return: the diff dataframe
    :raises ValueError: if ``target_tags`` isn't a valid list
    """
    if not target_tags:
        raise ValueError(f'Invalid target tags: {target_tags}. Must be a list of tags')

    previous = (
        previous.where(F.col(TAG_COLNAME).isin(target_tags))
        .withColumn(VALUES_COLNAME, F.sort_array(VALUES_COLNAME))
        .orderBy(
            WIKIID_COLNAME,
            PAGE_NAMESPACE_COLNAME,
            'page_id',
            TAG_COLNAME,
            VALUES_COLNAME,
        )
    )
    current = (
        current.where(F.col(TAG_COLNAME).isin(target_tags))
        .withColumn(VALUES_COLNAME, F.sort_array(VALUES_COLNAME))
        .orderBy(
            WIKIID_COLNAME,
            PAGE_NAMESPACE_COLNAME,
            'page_id',
            TAG_COLNAME,
            VALUES_COLNAME,
        )
    )
    changed = current.subtract(previous).select(
        WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME
    )
    deleted = (
        previous.join(
            current,
            on=[WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME],
            how='left_anti',
        )
        .withColumn('values', F.array(F.lit('__DELETE_GROUPING__')))
        .select(
            WIKIID_COLNAME,
            PAGE_NAMESPACE_COLNAME,
            'page_id',
            TAG_COLNAME,
            VALUES_COLNAME,
        )
    )

    return changed.union(deleted).orderBy(
        WIKIID_COLNAME, PAGE_NAMESPACE_COLNAME, 'page_id', TAG_COLNAME, VALUES_COLNAME
    )


def build_search_index_delta(
    spark_session: SparkSession,
    tags: DataFrame,
    target_tags: list,
    snapshot: str,
    is_commons: bool = False,
) -> DataFrame:
    """Build the delta of a search index's weighted tags dataset as computed by
    :func:`compute_search_index_delta`.

    Commons isn't processed by default. Pass ``is_commons=True`` to do so.

    .. note::

        The delta is computed against the **latest** weekly snapshot
        of the production Cirrus search index weighted tags Hive table,
        see :const:`queries.cirrus_index_tags`.
        That snapshot is **1 day before** our current one.

    :param spark_session: an active Spark session
    :param tags: a dataframe of weighted tags
    :param target_tags: a list of tags to consider for delta computation
    :param snapshot: a current ``YYYY-MM-DD`` date
    :param is_commons: whether the input dataframe is of Commons tags
    :return: the delta dataframe
    """
    if not is_commons:
        tags = tags.where(F.col(WIKIID_COLNAME) != 'commonswiki')

    previous = load_cirrus_index_tags(
        spark_session, get_cirrus_index_snapshot(snapshot)
    )
    previous = prepare_cirrus_index_tags(previous)
    delta = compute_search_index_delta(previous, tags, target_tags)

    # Facilitate tests
    return delta.withColumn('snapshot', F.lit(snapshot))


def save_search_index_full(
    data: DataFrame, hive_db: str, snapshot: str, coalesce: int
) -> DataFrame:
    """Write the full state of a search index's weighted tags dataset to
    the Data Lake's :const:`SEARCH_INDEX_FULL_TABLE` Hive table.

    :param data: a dataframe of weighted tags
    :param hive_db: a output Hive database name
    :param snapshot: a ``YYYY-MM-DD`` date
    :param coalesce: an integer to control the amount of files per output partition.
      A higher value implies more files but a faster and lighter execution.
    :return: the full dataframe
    """
    full = data.withColumn('snapshot', F.lit(snapshot))
    save_table(full, hive_db, SEARCH_INDEX_FULL_TABLE, coalesce)

    return full


def add_null_missing_columns(suggestions: DataFrame) -> DataFrame:
    """Add the ``section_index`` and ``section_heading`` columns
    populated with ``null`` values when article-level image suggestions are passed.

    :param suggestions: a dataframe of image suggestions
    :return: the input dataframe with eventually missing columns added
    """
    if 'section_index' not in suggestions.columns:
        suggestions = suggestions.withColumn(
            'section_index', F.lit(None).cast(IntegerType())
        )
    if 'section_heading' not in suggestions.columns:
        suggestions = suggestions.withColumn(
            'section_heading', F.lit(None).cast(StringType())
        )

    return suggestions


def save_suggestions(
    suggestions: DataFrame, hive_db: str, snapshot: str, coalesce: int
) -> DataFrame:
    """Write the final image suggestions dataset to
    the Data Lake's :const:`SUGGESTIONS_TABLE` Hive table.

    :param suggestions: a dataframe of image suggestions
    :param hive_db: a output Hive database name
    :param snapshot: a ``YYYY-MM-DD`` date
    :param coalesce: an integer to control the amount of files per output partition.
      A higher value implies more files but a faster and lighter execution.
    :return: the final output dataframe. See the row example in :ref:`alis`
    """
    suggestions = (
        suggestions.withColumn('snapshot', F.lit(snapshot))
        .withColumnRenamed('item_id', 'page_qid')
        .select(
            'page_id',
            DATASET_ID_COLNAME,
            'image',
            ORIGIN_WIKI_COLNAME,
            'confidence',
            'found_on',
            'kind',
            'page_rev',
            'section_heading',
            'snapshot',
            'wiki',
            'page_qid',
            'section_index',
        )
    )

    save_table(
        suggestions,
        hive_db,
        SUGGESTIONS_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )

    # Returning this to make it testable
    return suggestions


def save_title_cache(
    suggestions: DataFrame, hive_db: str, snapshot: str, coalesce: int
) -> DataFrame:
    """Write Wikipedia article candidates for suggestions to
    the Data Lake's :const:`TITLE_CACHE_TABLE` Hive table.

    :param suggestions: a dataframe of image suggestions
    :param hive_db: a output Hive database name
    :param snapshot: a ``YYYY-MM-DD`` date
    :param coalesce: an integer to control the amount of files per output partition.
      A higher value implies more files but a faster and lighter execution.
    :return: the written dataframe of:

        - page_id (bigint) - article page ID
        - page_rev (bigint) - article page revision ID
        - title (string) - article page title
        - snapshot (string) - ``YYYY-MM-DD`` date
        - wiki (string) - wiki project
    """
    title_cache = (
        suggestions.withColumn('snapshot', F.lit(snapshot))
        .select('page_id', 'page_rev', 'title', 'snapshot', 'wiki')
        .distinct()
    )

    save_table(
        title_cache,
        hive_db,
        TITLE_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )

    # Returning this to make it testable
    return title_cache


def save_instanceof_cache(
    spark: SparkSession,
    suggestions: DataFrame,
    hive_db: str,
    snapshot: str,
    coalesce: int,
) -> DataFrame:
    """Write Wikipedia articles that are instances of Wikidata QIDs to
    the Data Lake's :const:`INSTANCEOF_CACHE_TABLE` Hive table.

    :param spark: an active Spark session
    :param suggestions: a dataframe of image suggestions
    :param hive_db: a output Hive database name
    :param snapshot: a ``YYYY-MM-DD`` date
    :param coalesce: an integer to control the amount of files per output partition.
      A higher value implies more files but a faster and lighter execution.
    :return: the written dataframe of:

        - wiki (string) - wiki project
        - page_id (bigint) - article page ID
        - page_rev (bigint) - article page revision ID
        - instance_of (array<string>) - Wikidata QIDs that are classes of the article
        - snapshot (string) - ``YYYY-MM-DD`` date
    """
    wikidata_items_with_p31 = load_wikidata_items_with_p31(spark, snapshot)
    instanceof_cache = (
        suggestions
        # Left join so that we have an entry for every article even if it has no instance_of value
        .join(wikidata_items_with_p31, on=['item_id'], how='left')
        .groupBy('wiki', 'page_id', 'page_rev', 'item_id', 'section_heading')
        .agg(F.array_distinct(F.collect_list('value')).alias('instance_of'))
        .withColumn('snapshot', F.lit(snapshot))
        .select('wiki', 'page_id', 'page_rev', 'instance_of', 'snapshot')
    )

    save_table(
        instanceof_cache,
        hive_db,
        INSTANCEOF_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki'],
    )

    # Returning this to make it testable
    return instanceof_cache


def save_table(
    data: DataFrame,
    db_name: str,
    table_name: str,
    coalesce: int,
    partition_columns: list = ['snapshot'],
    mode: str = 'append',
) -> None:
    """Write a dataframe to a Hive table.

    :param data: a dataframe to be written
    :param db_name: a output Hive database name
    :param table_name: a output Hive table name
    :param coalesce: an integer to control the amount of files per output partition.
      A higher value implies more files but a faster and lighter execution.
    :param partition_columns: a list of partition columns
    :param mode: one of the following writing modes:

        - `append` - append contents of this :class:`DataFrame` to existing data
        - `overwrite` - overwrite existing data
        - `error` or `errorifexists` - throw an exception if data already exists
        - `ignore` - silently ignore this operation if data already exists
    """
    # Write what's happening to the spark context so we can monitor it
    target = f'{db_name}.{table_name}'
    partition = ','.join(partition_columns)
    data.rdd.context.setLocalProperty(
        'callSite.short', f'{mode} "{target}" table, partition by "{partition}"'
    )

    data.coalesce(coalesce).write.saveAsTable(
        target, partitionBy=partition_columns, mode=mode
    )


def create_dataset_id() -> str:  # pragma: no cover
    """Mint a dataset unique ID, to be used by
    Wikimedia Foundation's Cassandra instance.

    :return: the unique dataset ID
    """
    return str(uuid.uuid1())


def load_lead_images(
    spark: SparkSession, hive_db: str, weekly_snapshot: str
) -> DataFrame:
    """Gather lead images of Wikipedia articles.

    :param spark: an active Spark session
    :param hive_db: a Data Lake's `Hive <https://hive.apache.org/>`_ database name
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of:

        - item_id (string) - Wikidata QID
        - page_id (bigint) - Commons image page ID
        - found_on (array<string>) - wikis where the image was found
    """
    return spark.sql(queries.lead_images.format(hive_db, weekly_snapshot))


def save_suggestions_and_cache(
    spark: SparkSession,
    suggestions: DataFrame,
    hive_db: str,
    snapshot: str,
    coalesce: int,
) -> None:
    save_suggestions(suggestions, hive_db, snapshot, coalesce)
    save_title_cache(suggestions, hive_db, snapshot, coalesce)
    save_instanceof_cache(spark, suggestions, hive_db, snapshot, coalesce)


def load_wikidata_item_page_links(
    spark: SparkSession, weekly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load Wikidata page links through the
    :const:`image_suggestions.queries.wikidata_item_page_links` Data Lake query.

    :param spark: an active Spark session
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of QIDs, wikis, and page IDs, and wikis
    """
    return spark.sql(queries.wikidata_item_page_links.format(weekly_snapshot))


def build_base_arg_parser(description: str) -> ArgumentParser:
    parser = ArgumentParser(description=description)
    parser.add_argument('hive_db', help='Output Hive database')
    parser.add_argument('snapshot', metavar='YYYY-MM-DD', help='Weekly snapshot date')
    parser.add_argument(
        'coalesce',
        metavar='N',
        type=int,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution.'
        ),
    )

    return parser


def build_spark_session(
    environment: str = 'prod', dev_spark_settings: dict = DEV_SPARK_SETTINGS
) -> SparkSession:
    if environment == 'prod':
        return SparkSession.builder.getOrCreate()
    elif environment == 'dev':
        from wmfdata.spark import create_session

        return create_session(
            app_name='image-suggestions-dev',
            ship_python_env=True,
            extra_settings=dev_spark_settings,
        )
    else:
        raise ValueError(f"Invalid environment: {environment}. Must be 'prod' or 'dev'")
