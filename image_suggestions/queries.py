#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A set of `Spark-flavoured <https://spark.apache.org/docs/3.1.2/sql-ref-syntax.html>`_ SQL queries
that gather relevant data from the Wikimedia Foundation's
`Analytics Data Lake <https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake>`_.
"""

WIKIDATA_TABLE = 'image_suggestions_wikidata_data'
LEAD_IMAGES_TABLE = 'image_suggestions_lead_image_data'

#: Compute the amount of article pages per wiki, redirects excluded.
wiki_sizes = """SELECT wiki_db, COUNT(*) AS size
FROM wmf_raw.mediawiki_page
WHERE snapshot='{}'
AND page_namespace=0
AND page_is_redirect=0
GROUP BY wiki_db
"""

#: Gather `image <https://www.wikidata.org/wiki/Property:P18>`_ Wikidata
#: `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_.
#:
#: `regexp_extract <https://spark.apache.org/docs/3.1.2/api/sql/#regexp_extract>`_
#: removes wrapping quotes from string values and
#: replaces spaces with underscores to match image file names in
#: `page_title <https://www.mediawiki.org/wiki/Manual:Page_table#page_title>`_'s format.
wikidata_items_with_P18 = """SELECT id AS item_id,
replace(regexp_extract(claim.mainSnak.datavalue.value, '^"(.*)"$', 1), ' ', '_') AS value
FROM wmf.wikidata_entity
LATERAL VIEW OUTER explode(claims) AS claim
WHERE snapshot='{}'
AND typ='item'
AND claim.mainSnak.property='P18'
"""

#: Gather `Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata
#: `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_.
#:
#: `regexp_extract <https://spark.apache.org/docs/3.1.2/api/sql/#regexp_extract>`_
#: removes wrapping quotes from string values and
#: replaces spaces with underscores to match category names elsewhere.
wikidata_items_with_P373 = """SELECT id AS item_id,
replace(regexp_extract(claim.mainSnak.datavalue.value, '^"(.*)"$', 1), ' ', '_') AS value
FROM wmf.wikidata_entity
LATERAL VIEW OUTER explode(claims) AS claim
WHERE snapshot='{}'
AND typ='item'
AND claim.mainSnak.property='P373'
"""

#: Gather `instance of <https://www.wikidata.org/wiki/Property:P31>`_ Wikidata
#: `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_.
#:
#: `from_json <https://spark.apache.org/docs/3.1.2/api/sql/#from_json>`_
#: extracts Wikidata `QIDs <https://www.wikidata.org/wiki/WD:GLOSS#QID>`_,
#: which are stored as JSON strings in claims.
wikidata_items_with_P31 = """SELECT id AS item_id,
from_json(claim.mainSnak.dataValue.value, 'entityType STRING, numericId INT, id STRING').id AS value
FROM wmf.wikidata_entity
LATERAL VIEW OUTER explode(claims) AS claim
WHERE snapshot='{}'
AND typ='item'
AND claim.mainSnak.property='P31'
"""

#: Gather Commons *depicts* `statements <https://www.wikidata.org/wiki/WD:GLOSS#Statement>`_.
#:
#: `depicts <https://www.wikidata.org/wiki/Property:P180>`_,
#: `is digital representation of <https://www.wikidata.org/wiki/Property:P6243>`_
#: Wikidata properties are all used to represent similar information.
commons_pages_with_depicts = """SELECT DISTINCT
from_json(statement.mainsnak.datavalue.value, 'entityType STRING, numericId INT, id STRING').id AS item_id,
SUBSTRING(id, 2) AS page_id,
statement.mainsnak.property AS property_id
FROM structured_data.commons_entity
LATERAL VIEW OUTER explode(statements) AS statement
WHERE snapshot='{}'
AND statement.mainsnak.property IN ('P180', 'P6243')
"""

# NOTE Not a query to the Data Lake, so out of public documentation
# NOTE This f-string will need a `str.format` call for the DB name & snapshot
lead_images = f"""SELECT *
FROM {{}}.{LEAD_IMAGES_TABLE}
WHERE snapshot='{{}}'
"""

# NOTE Not a query to the Data Lake, so out of public documentation
# NOTE This f-string will need a `str.format` call for the DB name & snapshot
wikidata = f"""SELECT *
FROM {{}}.{WIKIDATA_TABLE}
WHERE snapshot='{{}}'
"""

#: Gather Commons file page IDs and titles.
commons_file_pages = """SELECT page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE snapshot='{}'
AND wiki_db='commonswiki'
AND page_namespace=6
AND page_is_redirect=0
"""

#: Gather file page IDs and titles locally stored in wikis.
local_images = """SELECT wiki_db, page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE snapshot='{}'
AND wiki_db!='commonswiki'
AND page_namespace=6
AND page_is_redirect=0
"""

#: Gather Commons categories linked to file page IDs.
category_links = """SELECT cl_from AS page_id, cl_to AS cat_title
FROM wmf_raw.mediawiki_categorylinks
WHERE snapshot='{}'
AND wiki_db='commonswiki'
AND cl_type='file'
"""

#: Gather Commons categories used by less than 100 k pages.
categories = """SELECT cat_title, cat_pages
FROM wmf_raw.mediawiki_category
WHERE snapshot='{}'
AND wiki_db='commonswiki'
AND cat_pages<100000
AND cat_pages>0
"""

#: Gather `article <https://en.wikipedia.org/wiki/WP:NS0>`_ pages of all wikis but Commons.
non_commons_main_pages = """SELECT wiki_db, page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE snapshot='{}'
AND wiki_db!='commonswiki'
AND page_namespace=0
AND page_is_redirect=0
"""

#: Gather all page links.
pagelinks = """SELECT pl.wiki_db, lt_title AS to_title, pl_from AS from_id
FROM wmf_raw.mediawiki_pagelinks pl
INNER JOIN wmf_raw.mediawiki_private_linktarget lt
ON pl.snapshot=lt.snapshot
AND pl.wiki_db=lt.wiki_db
AND pl.pl_target_id=lt.lt_id
WHERE pl.snapshot='{}'
"""

#: Gather page IDs with lead image file names from all wikis but Commons.
pages_with_lead_images = """SELECT wiki_db, pp_page AS page_id, pp_value AS lead_image_title
FROM wmf_raw.mediawiki_page_props
WHERE snapshot='{}'
AND wiki_db!='commonswiki'
AND pp_propname='page_image_free'
"""

#: Gather page IDs linked to Wikidata `QIDs <https://www.wikidata.org/wiki/WD:GLOSS#QID>`_.
wikidata_item_page_links = """SELECT item_id, wiki_db, page_id
FROM wmf.wikidata_item_page_link
WHERE snapshot='{}'
AND page_namespace=0
"""

#: Gather image file names linked to `article <https://en.wikipedia.org/wiki/WP:NS0>`_
#: pages of all wikis but Commons.
imagelinks = """SELECT wiki_db, il_from AS article_id, il_to AS image_title
FROM wmf_raw.mediawiki_imagelinks
WHERE snapshot='{}'
AND wiki_db!='commonswiki'
AND il_from_namespace=0
"""

#: Gather page IDs with their latest revisions.
latest_revisions = """SELECT wiki_db, rev_page AS page_id, MAX(rev_id) AS rev_id
FROM wmf_raw.mediawiki_revision
WHERE snapshot='{}'
GROUP BY wiki_db, rev_page
"""

# NOTE Not a query to the Data Lake, so out of public documentation
suggestions = """SELECT DISTINCT wiki, page_id
FROM {}.{}
WHERE snapshot='{}'
AND section_heading {}
ORDER BY page_id
"""

#: Gather image suggestions' user feedback.
suggestions_with_feedback = """SELECT wiki, page_id, filename
FROM event_sanitized.mediawiki_image_suggestions_feedback
WHERE datacenter!=''
AND year>=2022 AND month>0 AND day>0 AND hour<24
AND (is_rejected=True OR is_accepted=True)
"""

#: Gather Cirrus search index weighted tags available in production.
#: Used as a previous state to compute the search index delta.
#: The expected snapshot is `YYYYMMDD`.
cirrus_index_tags = """SELECT wiki, namespace, page_id, weighted_tags
FROM discovery.cirrus_index_without_content
WHERE cirrus_replica='codfw'
AND snapshot='{}'
"""
