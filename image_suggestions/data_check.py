#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Health checks on most recent data written by the image suggestions pipeline"""

import json
import ssl

from urllib.request import Request, urlopen

from prometheus_client import CollectorRegistry, Gauge, push_to_gateway


def configure_ssl_contex() -> ssl.SSLContext:
    context = ssl.SSLContext(ssl.PROTOCOL_TLS)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations(cafile='/etc/ssl/certs/ca-certificates.crt')
    return context


def collect_hasrecommendation_for_article(metrics: dict) -> None:
    search_url = 'https://mw-api-int-ro.discovery.wmnet:4446/w/index.php?search=hasrecommendation%3Aimage&title=Special%3ASearch&ns0=1&cirrusDumpResult'
    wikis = ['en', 'pt', 'id', 'ru']
    for wiki in wikis:
        data = make_request(search_url, wiki)
        metrics['hasrecommendation_for_article_count'].labels(wiki=wiki).set(
            data['__main__']['result']['hits']['total']['value']
        )


def make_request(search_url: str, wiki: str) -> dict:
    request = Request(search_url)
    request.add_header('Host', '{}.wikipedia.org'.format(wiki))
    response = urlopen(request, context=configure_ssl_contex())
    data = json.load(response)

    return data  # type: ignore[no-any-return]


def collect_hasrecommendation_for_section(metrics: dict) -> None:
    search_url = 'https://mw-api-int-ro.discovery.wmnet:4446/w/index.php?search=hasrecommendation%3Aimage_section&title=Special%3ASearch&ns0=1&cirrusDumpResult'
    wikis = ['en', 'pt', 'id', 'ru']
    for wiki in wikis:
        data = make_request(search_url, wiki)
        metrics['hasrecommendation_for_section_count'].labels(wiki=wiki).set(
            data['__main__']['result']['hits']['total']['value']
        )


def register_metrics(registry: CollectorRegistry) -> dict:
    return {
        'hasrecommendation_for_article_count': Gauge(
            'image_suggestions_elasticsearch_hasrecommendation_for_article_total',
            'total number of article image suggestions in elasticsearch',
            ['wiki'],
            registry=registry,
        ),
        'hasrecommendation_for_section_count': Gauge(
            'image_suggestions_elasticsearch_hasrecommendation_for_section_total',
            'total number of section image suggestions in elasticsearch',
            ['wiki'],
            registry=registry,
        ),
    }


def main() -> None:
    registry = CollectorRegistry()
    metrics = register_metrics(registry)
    collect_hasrecommendation_for_article(metrics)
    collect_hasrecommendation_for_section(metrics)
    push_to_gateway(
        'prometheus-pushgateway.discovery.wmnet',
        job='image_suggestions_pipeline',
        registry=registry,
    )


if __name__ == '__main__':
    main()
