#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Build Wikidata and lead images datasets for consumption by
ALIS, SLIS, and Commons' search index.

The former dataset is of `image <https://www.wikidata.org/wiki/Property:P18>`_ and
`Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata
`claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_,
complemented with `confidence scores <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline#Confidence_score>`_.

The latter is of Wikipedia article lead images, complemented with image
`relevance scores <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline#Relevance_score>`_.
"""

import argparse

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from image_suggestions import queries, shared


# Spark settings for development on stat boxes.
# They should mirror Wikidata & lead images' production DAG properties:
# https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/wikidata_and_lead_images_dag.py
DEV_SPARK_SETTINGS = {
    'spark.driver.cores': 2,
    'spark.driver.memory': '8g',
    'spark.executor.cores': 4,
    'spark.executor.memory': '8g',
    'spark.dynamicAllocation.maxExecutors': 64,
    'spark.executor.memoryOverhead': '12g',
    'spark.sql.shuffle.partitions': 256,
    'spark.sql.sources.partitionOverwriteMode': 'dynamic',
}

MAX_SCORE = 1000
# https://phabricator.wikimedia.org/T314120
QID_THRESHOLD = 99
# https://phabricator.wikimedia.org/T325629
FREQUENTLY_UPDATED_PAGE_QIDS = ['Q5296']  # Main page


def load_wikidata_items_with_P18(
    spark: SparkSession, weekly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load `image <https://www.wikidata.org/wiki/Property:P18>`_ Wikidata claims
    through the :const:`image_suggestions.queries.wikidata_items_with_P18` Data Lake query.

    :param spark: an active Spark session
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of Wikidata `QIDs <https://www.wikidata.org/wiki/WD:GLOSS#QID>`_
      and Commons image file names
    """
    return spark.sql(queries.wikidata_items_with_P18.format(weekly_snapshot))


def load_wikidata_items_with_P373(
    spark: SparkSession, weekly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load `Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata claims
    through the :const:`image_suggestions.queries.wikidata_items_with_P373` Data Lake query.

    :param spark: an active Spark session
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of Wikidata QIDs
      and Commons image file names
    """
    return spark.sql(queries.wikidata_items_with_P373.format(weekly_snapshot))


def load_commons_file_pages(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    """Load Commons file pages
    through the :const:`image_suggestions.queries.commons_file_pages` Data Lake query.

    :param spark: an active Spark session
    :param monthly_snapshot: a ``YYYY-MM`` date
    :return: the dataframe of Commons file page IDs and titles
    """
    return spark.sql(queries.commons_file_pages.format(monthly_snapshot))


def load_categories(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.categories.format(monthly_snapshot))


def load_category_links(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.category_links.format(monthly_snapshot))


def load_non_commons_main_pages(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.non_commons_main_pages.format(monthly_snapshot))


def load_pagelinks(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.pagelinks.format(monthly_snapshot))


def load_lead_images(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.pages_with_lead_images.format(monthly_snapshot))


def build_wikidata(
    spark: SparkSession,
    weekly_snapshot: str,
    commons_file_pages: DataFrame,
    wikidata_items_with_P18: DataFrame,
    wikidata_items_with_P373: DataFrame,
) -> DataFrame:
    """Build the dataset of `image <https://www.wikidata.org/wiki/Property:P18>`_ and
    `Commons category <https://www.wikidata.org/wiki/Property:P373>`_ Wikidata
    `claims <https://www.wikidata.org/wiki/WD:GLOSS#Claim>`_.

    Claims are complemented with `confidence scores <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline#Confidence_score>`_.
    The dataset is stored in the :const:`image_suggestions.shared.WIKIDATA_DATA` Hive table.

    :param spark: an active Spark session
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :param commons_file_pages: a dataframe of Commons file pages
    :param wikidata_items_with_P18: a dataframe of Wikidata image claims
    :param wikidata_items_with_P373: a dataframe of Wikidata Commons category claims
    :return: the dataframe of Wikidata claims and their confidence scores
    """
    monthly_snapshot = shared.get_monthly_snapshot(weekly_snapshot)
    commons_with_reverse_p18 = gather_commons_with_reverse_p18(
        commons_file_pages, wikidata_items_with_P18
    )
    commons_with_reverse_p373 = gather_commons_with_reverse_p373(
        spark, monthly_snapshot, commons_file_pages, wikidata_items_with_P373
    )

    return commons_with_reverse_p18.union(commons_with_reverse_p373)


def gather_commons_with_reverse_p18(
    commons_file_pages: DataFrame, wikidata_items_with_P18: DataFrame
) -> DataFrame:
    return (
        commons_file_pages.withColumn(shared.TAG_COLNAME, F.lit(shared.P18_TAG))
        # Set a constant score for P18 values to Elasticsearch's maximum, otherwise it defaults to 1
        .withColumn(shared.SCORE_COLNAME, F.lit(MAX_SCORE))
        .join(
            wikidata_items_with_P18,
            on=[commons_file_pages.page_title == wikidata_items_with_P18.value],
            how='inner',
        )
        .select(
            commons_file_pages.page_id,
            wikidata_items_with_P18.item_id,
            shared.TAG_COLNAME,
            shared.SCORE_COLNAME,
        )
    )


def gather_categories_with_links(
    spark: SparkSession, monthly_snapshot: str
) -> DataFrame:
    categories = load_categories(spark, monthly_snapshot)
    category_links = load_category_links(spark, monthly_snapshot)

    return categories.join(category_links, on=['cat_title'], how='inner')


def gather_commons_with_reverse_p373(
    spark: SparkSession,
    monthly_snapshot: str,
    commons_file_pages: DataFrame,
    wikidata_items_with_P373: DataFrame,
) -> DataFrame:
    all_commons_with_reverse_p373 = gather_all_commons_with_reverse_p373(
        spark, monthly_snapshot, commons_file_pages, wikidata_items_with_P373
    )
    return filter_commons_with_reverse_p373(all_commons_with_reverse_p373)


def gather_all_commons_with_reverse_p373(
    spark: SparkSession,
    monthly_snapshot: str,
    commons_file_pages: DataFrame,
    wikidata_items_with_P373: DataFrame,
) -> DataFrame:
    cats_with_links = gather_categories_with_links(spark, monthly_snapshot)
    files_with_categories = commons_file_pages.join(
        cats_with_links,
        on=[commons_file_pages.page_id == cats_with_links.page_id],
        how='inner',
    )

    return (
        files_with_categories.withColumn(shared.TAG_COLNAME, F.lit(shared.P373_TAG))
        .join(
            wikidata_items_with_P373,
            on=[cats_with_links.cat_title == wikidata_items_with_P373.value],
            how='inner',
        )
        .withColumn(
            shared.SCORE_COLNAME,
            # Compute a relevance score based on the following intuition:
            # an image in a category with few members is more important than one in a category with many members.
            # Given a Wikidata category item, its score is inversely proportional
            # to the logarithm of the total images it holds.
            F.round(
                (1 / F.log(files_with_categories.cat_pages + 1)) * MAX_SCORE / 1.443
            ).cast('int'),
        )
        .select(
            commons_file_pages.page_id,
            wikidata_items_with_P373.item_id,
            shared.TAG_COLNAME,
            shared.SCORE_COLNAME,
        )
    )


def filter_commons_with_reverse_p373(
    all_commons_with_reverse_p373: DataFrame,
) -> DataFrame:
    # If a commons file has its own wikidata item (via P18), then on wikidata its commons category is often (or
    # always?) used to indicate the category the wikidata item is *in* rather than the category containing images
    # *relevant to* the wikidata item
    #
    # This sense of the use of commons category is not useful for search - if I'm searching for an image for "Christ
    # washing the feet of the apostles" I don't want other images from Category:Images_from_the_Royal_Library_of_Belgium
    # in the results. Similarly for image suggestions other images from that category are not appropriate suggestions
    # for an article about that particular artwork.
    #
    # It's impossible to tell definitively which meaning of the P373 property is being used. The best guess we can
    # make at the moment is that if an image is in a commons category that is the P373 value for >99 different wikidata
    # items, then the P373 value is probably being used in the sense of "the category the wikidata item is in" rather
    # than "the category containing images related to the wikidata item", and therefore we should ignore it.
    #
    # See https://phabricator.wikimedia.org/T314120

    images_to_filter = (
        all_commons_with_reverse_p373.select('item_id', 'page_id')
        .groupBy('page_id')
        .agg(F.count('item_id').alias('wikidata_item_count'))
        .filter(F.col('wikidata_item_count') > 99)
        .select('page_id')
    )

    return all_commons_with_reverse_p373.join(
        images_to_filter, on=['page_id'], how='left_anti'
    )


def gather_articles_with_lead_images(
    spark: SparkSession, weekly_snapshot: str, monthly_snapshot: str
) -> DataFrame:
    return filter_articles_with_lead_images(
        gather_all_articles_with_lead_images(spark, weekly_snapshot, monthly_snapshot)
    )


def gather_all_articles_with_lead_images(
    spark: SparkSession, weekly_snapshot: str, monthly_snapshot: str
) -> DataFrame:
    commons_file_pages = load_commons_file_pages(spark, monthly_snapshot)
    pages_with_lead_images = load_lead_images(spark, monthly_snapshot)
    non_commons_main_pages = load_non_commons_main_pages(spark, monthly_snapshot)
    wikidata_item_page_links = shared.load_wikidata_item_page_links(
        spark, weekly_snapshot
    )

    return (
        commons_file_pages.join(
            pages_with_lead_images,
            on=[
                pages_with_lead_images.lead_image_title == commons_file_pages.page_title
            ],
            how='inner',
        )
        # Need to join the `page` table to get the title of the page containing the lead image
        .join(
            non_commons_main_pages,
            on=[
                pages_with_lead_images.page_id == non_commons_main_pages.page_id,
                pages_with_lead_images.wiki_db == non_commons_main_pages.wiki_db,
            ],
            how='inner',
        )
        .join(
            wikidata_item_page_links,
            on=[
                non_commons_main_pages.page_id == wikidata_item_page_links.page_id,
                non_commons_main_pages.wiki_db == wikidata_item_page_links.wiki_db,
            ],
            how='inner',
        )
        .select(
            pages_with_lead_images.wiki_db.alias('article_wiki'),
            non_commons_main_pages.page_title.alias('article_title'),
            wikidata_item_page_links.item_id,
            commons_file_pages.page_id.alias('commons_page_id'),
        )
    )


def filter_articles_with_lead_images(
    all_articles_with_lead_images: DataFrame,
    qid_threshold: int = QID_THRESHOLD,
    frequently_updated_page_qids: list = FREQUENTLY_UPDATED_PAGE_QIDS,
) -> DataFrame:
    # Apply 2 filters to the dataframe of articles with lead images:
    #   1. Wikidata QIDs of frequently updated pages - https://phabricator.wikimedia.org/T325629
    #   2. lead images with more than `qid_threshold` Wikidata QIDs - https://phabricator.wikimedia.org/T314120
    #
    # Some images are used as the lead image on tens or even hundreds of thousands of pages - for example
    # there is an svg USA map that's used as lead image on >400k wiki articles about geographical districts in the US.
    #
    # This is not actually useful for users - I probably don't want a USA map as result in an image search
    # for "Sowbelly ridge" - and also clogs up the commons search index doc for the image with thousands of
    # `weighted_tags` entries containing the wikidata id of each wiki article.
    #
    # To solve this and prevent the commonswiki_file index from being overwhelmed we'll filter out rows from this
    # dataframe that have >99 wikidata ids for a single image.
    #
    # In practical terms this means that if an image is a lead image for a set of articles N, and if the set N contains
    # articles with >99 distinct wikidata ids, then the image is likely to be too general to be useful as a search
    # result or an image suggestion, and so we ignore it.
    images_to_filter = (
        all_articles_with_lead_images.select('item_id', 'commons_page_id')
        .groupBy('commons_page_id')
        .agg(F.count('item_id').alias('wikidata_item_count'))
        .filter(F.col('wikidata_item_count') > qid_threshold)
        .select('commons_page_id')
    )

    return (
        all_articles_with_lead_images
        # Filter 1
        .filter(~F.col('item_id').isin(frequently_updated_page_qids))
        # Filter 2
        .join(images_to_filter, on=['commons_page_id'], how='left_anti')
    )


def add_link_counts(
    spark: SparkSession, monthly_snapshot: str, articles_with_lead_images: DataFrame
) -> DataFrame:
    pagelinks = load_pagelinks(spark, monthly_snapshot)

    link_counts_by_wiki = (
        articles_with_lead_images.join(
            pagelinks,
            on=[
                articles_with_lead_images.article_title == pagelinks.to_title,
                articles_with_lead_images.article_wiki == pagelinks.wiki_db,
            ],
            how='inner',
        )
        .groupBy('article_wiki', 'article_title', 'item_id', 'commons_page_id')
        .agg(F.count(pagelinks.from_id).alias('incoming_link_count'))
    )

    return (
        link_counts_by_wiki.groupBy('item_id', 'commons_page_id')
        .agg(
            F.sum(link_counts_by_wiki.incoming_link_count).alias(
                'incoming_link_count_all'
            ),
            F.collect_set(link_counts_by_wiki.article_wiki).alias(
                shared.FOUND_ON_COLNAME
            ),
        )
        .select(
            'commons_page_id',
            'item_id',
            'incoming_link_count_all',
            shared.FOUND_ON_COLNAME,
        )
    )


def build_lead_images(spark: SparkSession, weekly_snapshot: str) -> DataFrame:
    """Build the dataset of Wikipedia article lead images, complemented with their
    `relevance scores <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline#Relevance_score>`_.

    The dataset is stored in the :const:`image_suggestions.shared.LEAD_IMAGE_DATA` Hive table.

    :param spark: an active Spark session
    :param weekly_snapshot: a ``YYYY-MM-DD`` date
    :return: the dataframe of Wikipedia article lead images and their relevance scores
    """
    monthly_snapshot = shared.get_monthly_snapshot(weekly_snapshot)
    articles = gather_articles_with_lead_images(
        spark, weekly_snapshot, monthly_snapshot
    )
    link_counts = add_link_counts(spark, monthly_snapshot, articles)

    lead_images = (
        link_counts.groupBy(
            'commons_page_id',
            'item_id',
            'incoming_link_count_all',
            shared.FOUND_ON_COLNAME,
        )
        .agg(
            # Compute image relevance scores:
            # the score is based on the amount of articles that link to articles holding a given lead image,
            # grouped by the target articles' Wikidata QID.
            # Let `Commons_File_A` be a lead image of `xxwiki/Article_X` and `yywiki/Article_Y`,
            # which map to the Wikidata QID `Q123`.
            # `Q123`'s score is proportional to the sum of incoming links for `Article_X` and `Article_Y`.
            #
            # Based on empirical evidence, we pick a threshold of `5,000` for incoming links
            # and a scaling factor of `0.2`.
            # Hence, when the sum of incoming links is > threshold,
            # we set the score to its maximum value, i.e., `MAX_SCORE`.
            # Otherwise, we set it to `incoming links * scaling factor`,
            # rounded to reduce variation across runs - https://phabricator.wikimedia.org/T384514.
            F.when(link_counts.incoming_link_count_all > 5_000, MAX_SCORE)
            .otherwise(
                (
                    (1 + F.floor(link_counts.incoming_link_count_all * F.lit(0.2) / 10))
                    * 10
                ).cast('int')
            )
            .alias(shared.SCORE_COLNAME)
        )
        .withColumn(shared.TAG_COLNAME, F.lit(shared.LEAD_IMAGE_TAG))
        .select(
            link_counts.commons_page_id.alias('page_id'),
            link_counts.item_id,
            shared.TAG_COLNAME,
            shared.SCORE_COLNAME,
            shared.FOUND_ON_COLNAME,
        )
    )

    return lead_images


def parse_args() -> argparse.Namespace:  # pragma: no cover
    description = 'Build Wikidata and lead image datasets'
    parser = shared.build_base_arg_parser(description)

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:
    hive_db = args.hive_db
    weekly = args.snapshot
    coalesce = args.coalesce
    monthly = shared.get_monthly_snapshot(weekly)

    commons = load_commons_file_pages(spark, monthly)
    p18 = load_wikidata_items_with_P18(spark, weekly)
    p373 = load_wikidata_items_with_P373(spark, weekly)
    wikidata = build_wikidata(spark, weekly, commons, p18, p373)
    shared.save_table(
        wikidata.withColumn('snapshot', F.lit(weekly)),
        hive_db,
        shared.WIKIDATA_TABLE,
        coalesce,
    )

    lead_images = build_lead_images(spark, weekly)
    shared.save_table(
        lead_images.withColumn('snapshot', F.lit(weekly)),
        hive_db,
        shared.LEAD_IMAGES_TABLE,
        coalesce,
    )


if __name__ == '__main__':
    args = parse_args()
    # Prod
    spark = shared.build_spark_session()
    # Dev
    # spark = shared.build_spark_session(environment='dev', dev_spark_settings=DEV_SPARK_SETTINGS)
    main(args, spark)
    spark.stop()
